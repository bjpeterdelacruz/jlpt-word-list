# JLPT Word List

JLPT Word List is a useful study tool for candidates who are studying for the Japanese-Language Proficiency Test (JLPT).
It contains Japanese words that will appear on the exam. The list is not exhaustive, however. English translations as
well as readings for the kanji characters are included.

The application uses a SQLite database that is preloaded with Japanese words and their English translations.

## Downloads

macOS: [Download](https://jlpt-word-list.s3.us-west-2.amazonaws.com/jlpt-word-list-macos.zip)

Windows: [Download](https://jlpt-word-list.s3.us-west-2.amazonaws.com/jlpt-word-list-windows.zip)

## Screenshots

![Launcher](https://bjdelacruz.dev/files/jlpt_word_list_launcher.png "Launcher")

![Standalone Client](https://bjdelacruz.dev/files/jlpt_word_list_main.png "Standalone Client")

![Server](https://bjdelacruz.dev/files/jlpt_word_list_server.png "Server")

![Connect to Server](https://bjdelacruz.dev/files/jlpt_word_list_networked_client.png "Connect to Server")

![Search](https://bjdelacruz.dev/files/jlpt_word_list_search.png "Search")

## User Guide

### macOS

1. Extract the contents of the `jlpt-word-list-macos.zip` file, which can be downloaded from the link above.

2. Open the Terminal, change into the directory to which the contents of the ZIP file were extracted, and then type
`./image/bin/jlpt-word-list`.

### Windows

1. Extract the contents of the `jlpt-word-list-windows.zip` file, which can be downloaded from the link above.

2. Navigate to the `image\bin` directory, and then double-click the `jlpt-word-list.bat` file.

## Developer Guide

1. Install the following:
    * Java 23 JDK or higher

2. Run `./gradlew clean build` to compile the application and build a distribution ZIP file for the current platform
(e.g., macOS or Windows). The ZIP file will be created in the `build` directory and named `image.zip`.

## Links
[Developer's Website](https://bjdelacruz.dev)
