package dev.bjdelacruz.jlpt.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import dev.bjdelacruz.commons.utils.UiUtils;
import dev.bjdelacruz.jlpt.client.main.ClientDbManager;
import dev.bjdelacruz.jlpt.client.main.ClientMain;
import dev.bjdelacruz.jlpt.client.main.NetworkedClientConfigDialogBox;
import dev.bjdelacruz.jlpt.common.AppConstants;
import dev.bjdelacruz.jlpt.common.db.SqliteDbManagerImpl;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.jlpt.common.i18n.LocaleChangeListener;
import dev.bjdelacruz.jlpt.common.i18n.SelectLanguageDialog;
import dev.bjdelacruz.jlpt.common.utils.ThreadUtils;
import dev.bjdelacruz.jlpt.server.main.ServerMain;
import dev.bjdelacruz.commons.ui.LoadingDialog;
import dev.bjdelacruz.commons.utils.FileUtils;

/**
 * A dialog box that allows a user to select either client mode or server mode for this application.
 * 
 * @author BJ Peter DeLaCruz
 */
final class ApplicationLauncher extends JFrame {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  static {
    dev.bjdelacruz.commons.utils.UiUtils.useSystemLookAndFeel(LOGGER);
    LOGGER.setLevel(Level.INFO);
  }

  private final JRadioButton standaloneClientRadioButton;
  private final JRadioButton networkedClientRadioButton;
  private final JRadioButton serverRadioButton;
  private final JLabel instructions;
  private final JButton btnSelectLanguage;
  private final JButton btnLaunch;

  /**
   * Creates a new ApplicationLauncher.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  ApplicationLauncher() {
    dev.bjdelacruz.jlpt.common.ui.UiUtils.setFrameProperties(this, LangUtils.getString("select_mode"));
    var innerPanel = new JPanel(new GridBagLayout());

    setLayout(new BorderLayout());

    standaloneClientRadioButton = new JRadioButton(LangUtils.getString("standalone"));
    standaloneClientRadioButton.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent event) {
        if (standaloneClientRadioButton.hasFocus()) {
          startStandaloneClient();
        }
      }

    });
    standaloneClientRadioButton.addFocusListener(new FocusAdapter() {

      @Override
      public void focusGained(FocusEvent event) {
        standaloneClientRadioButton.setSelected(true);
      }

    });
    standaloneClientRadioButton.setSelected(true);
    standaloneClientRadioButton.setMnemonic(KeyEvent.VK_C);
    var constraints = new GridBagConstraints();
    instructions = new JLabel(LangUtils.getString("instructions"));
    constraints.fill = GridBagConstraints.HORIZONTAL;
    innerPanel.add(instructions, constraints);
    constraints.gridy = 1;
    innerPanel.add(standaloneClientRadioButton, constraints);

    networkedClientRadioButton = new JRadioButton(LangUtils.getString("networked"));
    networkedClientRadioButton.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent event) {
        if (networkedClientRadioButton.hasFocus()) {
          setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
          UiUtils.closeFrame(ApplicationLauncher.this);
          new NetworkedClientConfigDialogBox();
        }
      }

    });
    networkedClientRadioButton.addFocusListener(new FocusAdapter() {

      @Override
      public void focusGained(FocusEvent event) {
        networkedClientRadioButton.setSelected(true);
      }

    });
    networkedClientRadioButton.setMnemonic(KeyEvent.VK_N);

    constraints.gridy = 2;
    innerPanel.add(networkedClientRadioButton, constraints);

    serverRadioButton = new JRadioButton(LangUtils.getString("server"));
    serverRadioButton.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent event) {
        if (serverRadioButton.hasFocus()) {
          setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
          UiUtils.closeFrame(ApplicationLauncher.this);
          new ServerMain();
        }
      }

    });
    serverRadioButton.addFocusListener(new FocusAdapter() {

      @Override
      public void focusGained(FocusEvent event) {
        serverRadioButton.setSelected(true);
      }

    });
    serverRadioButton.setMnemonic(KeyEvent.VK_S);
    constraints.gridy = 3;
    innerPanel.add(serverRadioButton, constraints);

    var group = new ButtonGroup();
    group.add(standaloneClientRadioButton);
    group.add(networkedClientRadioButton);
    group.add(serverRadioButton);

    var listener = new LocaleChangeListenerImpl();
    btnSelectLanguage = new JButton(LangUtils.getString("select_language"));
    btnSelectLanguage.addActionListener(_ -> new SelectLanguageDialog(listener).setVisible(true));

    btnLaunch = new JButton(LangUtils.getString("launch"));

    setupLaunchButton();

    constraints.gridy = 4;
    var buttonsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
    // buttonsPanel.add(btnSelectLanguage);
    buttonsPanel.add(btnLaunch);
    innerPanel.add(buttonsPanel, constraints);

    add(innerPanel, BorderLayout.CENTER);
    setPreferredSize(new Dimension(innerPanel.getPreferredSize().width + 50,
            innerPanel.getPreferredSize().height + 50));

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    UiUtils.setEscKey(this);
    pack();
    setVisible(true);
    setLocationRelativeTo(null);
  }

  /**
   * Sets up the Launch button (sets mnemonic, adds action listener, etc.).
   */
  private void setupLaunchButton() {
    btnLaunch.setMnemonic(KeyEvent.VK_L);

    btnLaunch.addActionListener(_ -> {
      var logs = "logs";
      var logsDir = new File(logs);
      if (!logsDir.exists() && !logsDir.mkdir()) {
        LOGGER.log(Level.WARNING, "Unable to create logs directory for log files.");
      }
      if (standaloneClientRadioButton.isSelected()) {
        var filename = logs + File.separator + "standalone.log";
        FileUtils.configureLogger(LOGGER, AppConstants.LOG_SIZE, AppConstants.LOG_ROTATION_COUNT, filename);
        startStandaloneClient();
      }
      else if (networkedClientRadioButton.isSelected()) {
        var filename = logs + File.separator + "networked.log";
        FileUtils.configureLogger(LOGGER, AppConstants.LOG_SIZE, AppConstants.LOG_ROTATION_COUNT, filename);
        var serverUrl = FileUtils.getProperty(AppConstants.NETWORKED_CLIENT_SERVER_LOCATION_KEY,
                AppConstants.PROPERTIES_FILE);
        var serverPort = FileUtils.getIntProperty(AppConstants.NETWORKED_CLIENT_SERVER_PORT_KEY,
                AppConstants.PROPERTIES_FILE);
        if (serverUrl.isEmpty() || serverUrl.get().isEmpty()) {
          setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
          UiUtils.closeFrame(ApplicationLauncher.this);
          new NetworkedClientConfigDialogBox();
        }
        else {
          startNetworkedClient(serverUrl.get(), serverPort);
        }
      }
      else if (serverRadioButton.isSelected()) {
        var filename = logs + File.separator + "server.log";
        FileUtils.configureLogger(LOGGER, AppConstants.LOG_SIZE, AppConstants.LOG_ROTATION_COUNT, filename);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        UiUtils.closeFrame(ApplicationLauncher.this);
        new ServerMain();
      }
    });
  }

  /**
   * Starts the application in standalone client mode.
   */
  private void startStandaloneClient() {
    var msg = "Initializing database...  Please wait.";
    var icon = dev.bjdelacruz.jlpt.common.ui.UiUtils.getJapaneseFlagIcon();
    var dialog = new LoadingDialog("JLPT Word List", msg, 250, 75, null, icon);
    SwingUtilities.invokeLater(() -> {
      dialog.pack();
      dialog.setLocationRelativeTo(null);
      dialog.setVisible(true);
    });
    ThreadUtils.getSharedThreadPool().execute(() -> {
      try {
        var databaseManager = new SqliteDbManagerImpl();
        var entries = databaseManager.getEntries();
        SwingUtilities.invokeLater(() -> {
          dialog.dispose();
          var clientMain = new ClientMain(databaseManager, entries);
          clientMain.setStandaloneStatus();
          setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
          UiUtils.closeFrame(ApplicationLauncher.this);
        });
      }
      catch (Exception e) {
        SwingUtilities.invokeLater(() -> {
          var msg1 = "There was a problem trying to initialize the database:\n\n" + e;
          JOptionPane.showMessageDialog(ApplicationLauncher.this, msg1, "Error",
              JOptionPane.ERROR_MESSAGE);
          LOGGER.log(Level.SEVERE,
                  String.format("There was a problem trying to initialize the database: %s", e.getMessage()), e);
        });
      }
    });
  }

  /**
   * Starts the application in networked client mode.
   * 
   * @param serverUrl The URL of the server.
   * @param serverPort The port on the server to which to connect.
   */
  private void startNetworkedClient(final String serverUrl, final Integer serverPort) {
    ThreadUtils.getSharedThreadPool().execute(() -> {
      try {
        var clientDbManager = new ClientDbManager(serverUrl, serverPort);
        var entries = clientDbManager.getEntries();
        SwingUtilities.invokeLater(() -> {
          var clientMain = new ClientMain(clientDbManager, entries);
          clientDbManager.setServerStatusListener(clientMain);
          clientDbManager.startCheckServerStatusTask();
          setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
          UiUtils.closeFrame(ApplicationLauncher.this);
        });
      }
      catch (Exception e) {
        SwingUtilities.invokeLater(() -> {
          var msg = "There was a problem trying to start the networked client:\n\n" + e;
          JOptionPane.showMessageDialog(ApplicationLauncher.this, msg, "Error",
              JOptionPane.ERROR_MESSAGE);
          setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
          UiUtils.closeFrame(ApplicationLauncher.this);
          new NetworkedClientConfigDialogBox();
        });
      }
    });
  }

  /**
   * This listener will change the language of the text in this dialog.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class LocaleChangeListenerImpl implements LocaleChangeListener {

    /** {@inheritDoc} */
    @Override
    public void localeChanged() {
      setTitle(LangUtils.getString("select_mode"));
      standaloneClientRadioButton.setText(LangUtils.getString("standalone"));
      networkedClientRadioButton.setText(LangUtils.getString("networked"));
      serverRadioButton.setText(LangUtils.getString("server"));
      instructions.setText(LangUtils.getString("instructions"));
      btnSelectLanguage.setText(LangUtils.getString("select_language"));
      btnLaunch.setText(LangUtils.getString("launch"));
      invalidate();
      validate();
      repaint();
    }

  }

  /**
   * Starts the application.
   * 
   * @param args None.
   */
  public static void main(String... args) {
    new ApplicationLauncher();
  }
}
