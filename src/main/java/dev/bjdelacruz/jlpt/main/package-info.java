/**
 * Contains the window used to select the mode in which to run the application (standalone
 * client, networked client, or server).
 */
package dev.bjdelacruz.jlpt.main;
