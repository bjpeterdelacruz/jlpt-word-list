package dev.bjdelacruz.jlpt.server.main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import dev.bjdelacruz.jlpt.common.db.AddRemoveServerRequest;
import dev.bjdelacruz.jlpt.common.db.Commands;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.db.EntryAlreadyExistsException;
import dev.bjdelacruz.jlpt.common.db.EntryDoesNotExistException;
import dev.bjdelacruz.jlpt.common.db.FindServerRequest;
import dev.bjdelacruz.jlpt.common.db.InvalidRegExPatternException;
import dev.bjdelacruz.jlpt.common.db.StaleEntryException;
import dev.bjdelacruz.jlpt.common.db.UpdateServerRequest;

/**
 * A Runnable that listens for requests from a client and processes them. One Runnable per client.
 * 
 * @author BJ Peter DeLaCruz
 */
final class ServerTask implements Runnable {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final DbManager databaseManager;
  private final Socket socket;
  private static final int ONLY_ONE_ENTRY_FOUND = 1;

  /**
   * Creates a new Task.
   * 
   * @param databaseManager The database manager.
   * @param socket The socket to which a client is connected.
   */
  ServerTask(DbManager databaseManager, Socket socket) {
    if (databaseManager == null) {
      throw new IllegalArgumentException("Database manager must not be null.");
    }
    if (socket == null) {
      throw new IllegalArgumentException("Socket must not be null.");
    }
    this.databaseManager = databaseManager;
    this.socket = socket;
  }

  /** {@inheritDoc} */
  @Override
  public void run() {
    try (var ostream = new ObjectOutputStream(socket.getOutputStream());
         var istream = new ObjectInputStream(socket.getInputStream())) {
      ostream.flush();
      while (true) {
        var request = istream.readObject();
        if (request == Commands.QUIT) {
          LOGGER.log(Level.INFO, "Client is quitting.");
          break;
        }
        else if (request instanceof AddRemoveServerRequest addRemoveRequest) {
          switch (addRemoveRequest.command()) {
            case ADD -> {
              databaseManager.addEntry(addRemoveRequest.entry());

              LOGGER.log(Level.INFO, "Successfully added entry to database. New entry: " + addRemoveRequest.entry() + ".");
            }
            case REMOVE -> {
              databaseManager.removeEntry(addRemoveRequest.entry());

              LOGGER.log(Level.INFO, "Successfully removed entry from database. Old entry: " + addRemoveRequest.entry() + ".");
            }
            default -> throw new IllegalArgumentException("Invalid command: " + addRemoveRequest.command());
          }
        }
        else if (request instanceof UpdateServerRequest updateRequest) {
          databaseManager.updateEntry(updateRequest.newEntry(), updateRequest.oldEntry());

          var msg = "Successfully updated entry in database. Old entry: " + updateRequest.oldEntry() + ". ";
          msg += "New entry: " + updateRequest.newEntry() + ".";
          LOGGER.log(Level.INFO, msg);
        }
        else if (request instanceof FindServerRequest findRequest) {
          var results = databaseManager.find(findRequest.regex());
          ostream.writeObject(results);

          var msg = "Processed search using regular expression: " + findRequest.regex() + ". Found ";
          msg += results.size() == ONLY_ONE_ENTRY_FOUND ? "1 entry." : results.size() + " entries.";
          LOGGER.log(Level.INFO, msg);
        }
        else if (request == Commands.GET) {
          var entries = databaseManager.getEntries();
          ostream.writeObject(entries);

          LOGGER.log(Level.INFO, "Retrieved list of entries. Total: " + entries.size() + ".");
        }
      }
    }
    catch (ClassNotFoundException | EntryAlreadyExistsException | EntryDoesNotExistException
           | InvalidRegExPatternException | IOException | StaleEntryException | SQLException e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
    }
  }

}
