package dev.bjdelacruz.jlpt.server.main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import dev.bjdelacruz.commons.ui.AppStatusBar;
import dev.bjdelacruz.jlpt.common.AppConstants;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.db.SqliteDbManagerImpl;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.jlpt.common.ui.PortTextField;
import dev.bjdelacruz.jlpt.common.ui.UiUtils;
import dev.bjdelacruz.jlpt.common.utils.ThreadUtils;
import dev.bjdelacruz.commons.utils.FileUtils;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * The main server window in which the user can load the database file and set the port
 * number for the server.
 * 
 * @author BJ Peter DeLaCruz
 */
public class ServerMain extends JFrame {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final JTextField portTextField;
  private final JButton btnStartServer;
  private final JButton btnStopServer;
  private DbManager dbManager;
  private ServerDbManager serverDbManager;
  private final JLabel statusLabel;
  private final JCheckBox autoStartCheckBox;

  /**
   * Creates and displays the main server window on the user's screen.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public ServerMain() {
    getContentPane().setLayout(new BorderLayout());
    UiUtils.setFrameProperties(this, LangUtils.getString("server_title"));

    var buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    var centerPanel = new JPanel(new GridBagLayout());

    btnStopServer = new JButton(LangUtils.getString("stop_server"));
    btnStopServer.addActionListener(_ -> {
      try {
        serverDbManager.shutdown();
        toggleComponents(true);
        setOfflineStatus();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      }
      catch (IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage());
      }
    });
    btnStopServer.setEnabled(false);

    btnStartServer = new JButton(LangUtils.getString("start_server"));
    btnStartServer.addActionListener(_ -> startServer());
    btnStartServer.setEnabled(false);

    buttonPanel.add(btnStartServer);
    buttonPanel.add(btnStopServer);

    var portPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    var portLabel = new JLabel(LangUtils.getString("port"));
    portPanel.add(portLabel);

    portTextField = new PortTextField(new CustomKeyAdapter());
    portPanel.add(portTextField);

    autoStartCheckBox = new JCheckBox(LangUtils.getString("autostart"));

    var constraints = new GridBagConstraints();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    centerPanel.add(portPanel, constraints);
    constraints.gridy = 1;
    centerPanel.add(autoStartCheckBox, constraints);
    constraints.gridy = 2;
    centerPanel.add(buttonPanel, constraints);
    getContentPane().add(centerPanel, BorderLayout.CENTER);

    var statusBar = new AppStatusBar();

    var westPanel = new JPanel();
    westPanel.setOpaque(false);
    var padding = new JLabel();
    statusLabel = new JLabel();
    westPanel.add(padding);
    westPanel.add(statusLabel);

    setOfflineStatus();
    statusBar.addComponent(westPanel, BorderLayout.WEST);
    getContentPane().add(statusBar, BorderLayout.SOUTH);

    addWindowListener(new ServerWindowListener());
    setSize(545, 175);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    dev.bjdelacruz.commons.utils.UiUtils.setEscKey(this);
    setLocationRelativeTo(null);

    var port = FileUtils.getIntProperty(AppConstants.SERVER_PORT_KEY, AppConstants.PROPERTIES_FILE);

    portTextField.setText(String.valueOf(port));
    boolean shouldAutoStart = FileUtils.getBooleanProperty(AppConstants.SERVER_AUTOSTART_KEY, AppConstants.PROPERTIES_FILE);
    if (shouldAutoStart) {
      autoStartCheckBox.setSelected(true);
      startServer();
    }
    else {
      btnStartServer.setEnabled(true);
    }

    setVisible(true);
  }

  /**
   * Sets the label to offline status.
   */
  private void setOfflineStatus() {
    statusLabel.setText(LangUtils.getString("server_is_offline"));
    statusLabel.setIcon(UiUtils.getOfflineIcon());
  }

  /**
   * Sets the label to online status.
   * 
   * @throws IOException If there are problems trying to get the IP address of this server.
   */
  private void setOnlineStatus() throws IOException {
    var msg = LangUtils.getString("server_is_online");
    msg += " " + InetAddress.getLocalHost().getHostAddress() + ":" + portTextField.getText();
    statusLabel.setText(msg);
    statusLabel.setIcon(UiUtils.getOnlineIcon());
  }

  /**
   * Toggles the state of the components in the window.
   * 
   * @param isEnabled True to enable the components (except the Stop Server button), false otherwise
   * (false to disable the Start Server button).
   */
  private void toggleComponents(boolean isEnabled) {
    portTextField.setEnabled(isEnabled);
    autoStartCheckBox.setEnabled(isEnabled);
    btnStartServer.setEnabled(isEnabled);
    btnStopServer.setEnabled(!isEnabled);
  }

  /**
   * Displays the Open File dialog box.
   */
  private void openFileDialogBox() {
    var chooser = new JFileChooser();
    var filter = new FileNameExtensionFilter("Database file (*.db)", "db");
    chooser.setFileFilter(filter);
    var returnValue = chooser.showOpenDialog(ServerMain.this);
    if (returnValue == JFileChooser.APPROVE_OPTION && !portTextField.getText().isEmpty()) {
      btnStartServer.setEnabled(true);
    }
  }

  /**
   * Starts the server.
   */
  @SuppressFBWarnings("SE_BAD_FIELD_STORE")
  private void startServer() {
    try {
      statusLabel.setText(LangUtils.getString("initializing_database"));
      statusLabel.setIcon(UiUtils.getInitIcon());
      portTextField.setEnabled(false);
      autoStartCheckBox.setEnabled(false);
      btnStartServer.setEnabled(false);
      btnStopServer.setEnabled(false);
      ThreadUtils.getSharedThreadPool().execute(() -> {
        try {
          dbManager = new SqliteDbManagerImpl();
          var port = Integer.parseInt(portTextField.getText());
          serverDbManager = new ServerDbManager(dbManager, port);
          serverDbManager.start();
          SwingUtilities.invokeLater(() -> {
            toggleComponents(false);
            try {
              setOnlineStatus();
            }
            catch (IOException e) {
              LOGGER.log(Level.SEVERE, e.getMessage());
            }
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            LOGGER.log(Level.INFO, "Successfully started server.\n   Port: " + port);
          });
        }
        catch (Exception e) {
          LOGGER.log(Level.SEVERE, String.format("Failed to initialize database: %s", e.getMessage()), e);
          var msg = "Failed to initialize database. Reason:\n\n" + e;
          SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(ServerMain.this, msg, "Error",
              JOptionPane.ERROR_MESSAGE));
        }
      });
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
    }
  }

  /**
   * A key adapter that will enable/disable the Start Server button and, depending on the state of
   * the Start Server button, either display the Open File dialog box or start the server.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class CustomKeyAdapter extends KeyAdapter {

    @Override
    public void keyReleased(KeyEvent event) {
      btnStartServer.setEnabled(!portTextField.getText().isEmpty());

      if (event.getKeyChar() == KeyEvent.VK_ENTER) {
        if (btnStartServer.isEnabled()) {
          startServer();
        }
        else {
          openFileDialogBox();
        }
      }
    }

  }

  /**
   * A window listener that will save the values in the text fields to a properties file before the
   * application closes.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class ServerWindowListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent event) {
      try {
        FileUtils.saveProperty(AppConstants.SERVER_PORT_KEY, portTextField.getText(), AppConstants.PROPERTIES_FILE);
        FileUtils.saveProperty(AppConstants.SERVER_AUTOSTART_KEY, autoStartCheckBox.isSelected() + "", AppConstants.PROPERTIES_FILE);
      }
      catch (IOException ioe) {
        LOGGER.log(Level.SEVERE, ioe.getMessage());
      }
    }

  }

}
