package dev.bjdelacruz.jlpt.server.main;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import dev.bjdelacruz.jlpt.common.db.DbManager;

/**
 * A server-side proxy that processes requests from the client.
 * 
 * @author BJ Peter DeLaCruz
 */
final class ServerDbManager {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final ExecutorService threadPool = Executors.newCachedThreadPool();
  private final DbManager databaseManager;

  private final ServerSocket serverSocket;

  /**
   * Creates a new ServerDbManager.
   * 
   * @param databaseManager The database manager.
   * @param port The port number used to connect to the server.
   * @throws IOException If there are problems trying to create the server socket.
   */
  ServerDbManager(DbManager databaseManager, int port) throws IOException {
    if (databaseManager == null) {
      throw new IllegalArgumentException("Database manager must not be null.");
    }
    if (port < 0 || port > 65_535) {
      throw new IllegalArgumentException("Port number must be between 0 and 65,535, inclusive.");
    }

    this.databaseManager = databaseManager;
    this.serverSocket = new ServerSocket(port);
  }

  /**
   * Starts a thread that will listen for connections from the client. When a request comes in, the
   * thread will hand it over to another thread that will process the request.
   */
  void start() {
    threadPool.execute(() -> {
      while (!threadPool.isShutdown()) {
        try {
          var socket = serverSocket.accept();
          LOGGER.log(Level.INFO, "A new client connected.");
          threadPool.execute(new ServerTask(databaseManager, socket));
        }
        catch (SocketException e) {
          if (e.getMessage().contains("socket closed")) {
            LOGGER.log(Level.INFO, e.getMessage());
          }
          else {
            LOGGER.log(Level.SEVERE, e.getMessage());
          }
          break;
        }
        catch (Exception e) {
          LOGGER.log(Level.SEVERE, e.getMessage());
          break;
        }
      }
    });
  }

  /**
   * Shuts down the thread pool.
   * 
   * @throws IOException If there are problems shutting down the server socket.
   */
  void shutdown() throws IOException {
    serverSocket.close();
    threadPool.shutdown();
  }

}
