package dev.bjdelacruz.jlpt.client.table;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JTable;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * An action that will copy a selected entry or entries to the clipboard.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class CopyToClipboardAction extends AbstractAction implements ClipboardOwner {

  private final JTable table;

  /**
   * Creates a new action that will copy the selected entry or entries to the clipboard.
   * 
   * @param table The table containing the entry or entries to copy.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public CopyToClipboardAction(JTable table) {
    super(LangUtils.getString("copy_to_clipboard"));
    if (table == null) {
      throw new IllegalArgumentException("Table must not be null.");
    }
    this.table = table;
  }

  /** {@inheritDoc} */
  @Override
  public void actionPerformed(ActionEvent event) {
    var entries = new ArrayList<JapaneseEntry>();
    for (var row : table.getSelectedRows()) {
      var model = (EntriesTableModel) table.getModel();
      entries.add(model.getEntryAt(row));
    }
    setClipboardContents(entries);
  }

  /** {@inheritDoc} */
  @Override
  public void lostOwnership(Clipboard clipboard, Transferable transferable) {
    // Do nothing.
  }

  /**
   * Sets the contents of the clipboard to the selected entry or entries.
   * 
   * @param entries The list of entries to copy to the clipboard.
   */
  private void setClipboardContents(List<JapaneseEntry> entries) {
    var builder = new StringBuilder();
    for (var index = 0; index < entries.size(); index++) {
      builder.append(entries.get(index).toString());
      if (index != entries.size() - 1) {
        builder.append(System.lineSeparator());
      }
    }
    var clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(new StringSelection(builder.toString()), this);
  }

}
