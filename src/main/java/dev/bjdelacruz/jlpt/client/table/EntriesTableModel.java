package dev.bjdelacruz.jlpt.client.table;

import java.util.ArrayList;
import java.util.List;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.commons.ui.CustomTableModel;

/**
 * A table model that displays Japanese words and their English meanings in a JTable.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class EntriesTableModel extends CustomTableModel<JapaneseEntry> {

  private final List<JapaneseEntry> rows;

  private static final int NUM_COLUMNS = 3;

  /**
   * Creates a new EntriesTableModel instance.
   * 
   * @param rows The rows containing entries used to populate the JTable.
   */
  public EntriesTableModel(List<JapaneseEntry> rows) {
    if (rows == null) {
      throw new IllegalArgumentException("Rows must not be null.");
    }
    this.rows = new ArrayList<>(rows);
  }

  /** {@inheritDoc} */
  @Override
  public int getColumnCount() {
    return NUM_COLUMNS;
  }

  /** {@inheritDoc} */
  @Override
  public int getRowCount() {
    return rows.size();
  }

  /** {@inheritDoc} */
  @Override
  public String getColumnName(int column) {
    return switch (column) {
      case 0 -> "Kana/Kanji";
      case 1 -> LangUtils.getString("reading");
      case 2 -> LangUtils.getString("english_definition");
      default -> throw new IllegalArgumentException("Invalid column index: " + column);
    };
  }

  /** {@inheritDoc} */
  @Override
  public Object getValueAt(int row, int column) {
    return switch (column) {
      case 0 -> rows.get(row).japanese();
      case 1 -> rows.get(row).reading();
      case 2 -> rows.get(row).english();
      default -> throw new IllegalArgumentException("Invalid column index: " + column);
    };
  }

  /** {@inheritDoc} */
  @Override
  public boolean isCellEditable(int row, int column) {
    return false;
  }

  /** @return A copy of the entries in this table model. */
  public List<JapaneseEntry> getEntries() {
    return new ArrayList<>(rows);
  }

  /** {@inheritDoc} */
  @Override
  public void addEntry(JapaneseEntry entry) {
    throw new UnsupportedOperationException("This method is not supported yet.");
  }

  /** {@inheritDoc} */
  @Override
  public JapaneseEntry getEntryAt(int row) {
    if (row < 0) {
      throw new IllegalArgumentException("Row must not be a negative number.");
    }
    return JapaneseEntry.from(rows.get(row));
  }

  /** {@inheritDoc} */
  @Override
  public int getRowIndexOf(JapaneseEntry entry) {
    throw new UnsupportedOperationException("This method is not supported yet.");
  }

}
