package dev.bjdelacruz.jlpt.client.table;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.commons.utils.FileUtils;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * An action that will export the contents of a table to a CSV file.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class ExportAction extends AbstractAction {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final JTable table;
  private final JComponent component;

  /**
   * Creates a new action that will export the contents of the table to a CSV file.
   * 
   * @param table The table whose contents are to be exported.
   * @param component The menu to which this action belongs.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public ExportAction(JTable table, JComponent component) {
    super(LangUtils.getString("export_to_csv"));
    if (table == null) {
      throw new IllegalArgumentException("Table must not be null.");
    }
    if (component == null) {
      throw new IllegalArgumentException("Component must not be null.");
    }
    this.table = table;
    this.component = component;
  }

  /** {@inheritDoc} */
  @Override
  public void actionPerformed(ActionEvent event) {
    var chooser = new JFileChooser();
    var filterText = "Comma-separated file (*.csv)";
    var filter = new FileNameExtensionFilter(filterText, "csv");
    chooser.setFileFilter(filter);
    var returnValue = chooser.showSaveDialog(component);
    if (returnValue == JFileChooser.APPROVE_OPTION) {
      try {
        var entries = ((EntriesTableModel) table.getModel()).getEntries();
        var lines = entries.stream().map(entry -> entry.japanese() + "," + entry.reading() + "," + entry.english()).collect(Collectors.toList());
        FileUtils.writeToFile(lines, chooser.getSelectedFile());
      }
      catch (IOException e) {
        LOGGER.log(Level.SEVERE, "There was a problem writing to the file: " + e.getMessage());
      }
    }
  }

}
