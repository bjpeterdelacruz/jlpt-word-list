package dev.bjdelacruz.jlpt.client.table;

import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A custom table that displays Japanese words and their English translations.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class EntriesTable extends JTable {

  private final JPopupMenu menu;

  /**
   * Creates a EntriesTable instance.
   * 
   * @param model The table model that contains Japanese words and their English meanings.
   */
  public EntriesTable(EntriesTableModel model) {
    super(model);
    if (model == null) {
      throw new IllegalArgumentException("Model must not be null.");
    }

    menu = new JPopupMenu();

    setShowVerticalLines(false);
    addMouseListener(new MouseInputAdapter() {

      @Override
      public void mouseClicked(MouseEvent event) {
        if (SwingUtilities.isRightMouseButton(event)) {
          var row = rowAtPoint(event.getPoint());
          if (row >= 0 && row < getRowCount()) {
            setRowSelectionInterval(row, row);
          }
          else {
            clearSelection();
          }

          menu.show(EntriesTable.this, event.getX(), event.getY());
        }
      }

    });
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  /** @return The right-click popup menu. */
  @SuppressFBWarnings("EI_EXPOSE_REP")
  public JPopupMenu getPopupMenu() {
    return menu;
  }

  /**
   * Selects the given entry in the table and then returns it.
   * 
   * @param entry The entry to select.
   * @return The entry if it is found in the table, or <code>null</code> if it is not found.
   */
  public JapaneseEntry selectEntry(JapaneseEntry entry) {
    if (entry == null) {
      return null;
    }

    var rowCount = getRowCount();
    for (var index = 0; index < rowCount; index++) {
      if (getValueAt(index, 0).toString().equals(entry.japanese())) {
        getSelectionModel().setSelectionInterval(index, index);
        return getEntry(index);
      }
      else if (getValueAt(index, 1).toString().equals(entry.reading())) {
        getSelectionModel().setSelectionInterval(index, index);
        return getEntry(index);
      }
      else if (getValueAt(index, 2).toString().equals(entry.english())) {
        getSelectionModel().setSelectionInterval(index, index);
        return getEntry(index);
      }
    }

    return null;
  }

  /**
   * Returns the entry at the given row.
   * 
   * @param row The row where the entry is located.
   * @return The entry at the given row, or <code>null</code> if it is not found.
   */
  public JapaneseEntry getEntry(int row) {
    if (row < 0) {
      throw new IllegalArgumentException("Row must not be a negative number.");
    }

    return getEntry(getValueAt(row, 0).toString());
  }

  /**
   * Returns the entry at the given row.
   * 
   * @param japaneseWord The Japanese word to use for the search.
   * @return The entry that contains the given Japanese word.
   */
  public JapaneseEntry getEntry(String japaneseWord) {
    if (japaneseWord == null || japaneseWord.isBlank()) {
      throw new IllegalArgumentException("Japanese word must not be null or an empty string.");
    }

    for (var entry : ((EntriesTableModel) getModel()).getEntries()) {
      if (entry.japanese().equals(japaneseWord)) {
        return entry;
      }
    }
    return null;
  }

}
