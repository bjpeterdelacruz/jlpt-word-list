package dev.bjdelacruz.jlpt.client.main;

/**
 * An enum that represents the current status of the server.
 * 
 * @author BJ Peter DeLaCruz
 */
enum ServerStatus {

  /** The server is online. */
  ONLINE,
  /** The server is offline. */
  OFFLINE

}
