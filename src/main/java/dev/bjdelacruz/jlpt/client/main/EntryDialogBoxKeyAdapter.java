package dev.bjdelacruz.jlpt.client.main;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * This abstract class contains shared code for toggling the OK button in an EntryDialogBox.
 */
public abstract class EntryDialogBoxKeyAdapter extends KeyAdapter {

    private final EntryDialogBox dialogBox;

    /**
     * Creates a new KeyAdapter for the given EntryDialogBox.
     *
     * @param dialogBox A dialog box in which a user can add or modify an entry.
     */
    @SuppressFBWarnings("CT_CONSTRUCTOR_THROW")
    protected EntryDialogBoxKeyAdapter(EntryDialogBox dialogBox) {
        if (dialogBox == null) {
            throw new IllegalArgumentException("Dialog box must not be null.");
        }
        this.dialogBox = dialogBox;
    }

    /** @return True if the OK button has been disabled, false otherwise. */
    boolean disabledOkButton() {
        if (dialogBox.getJapaneseWordText().isEmpty() || dialogBox.getReadingText().isEmpty()
                || dialogBox.getEnglishTranslationText().isEmpty()) {
            dialogBox.setOkButtonEnabled(false);
            return true;
        }
        return false;
    }

    /** @param event A key event. */
    void enableOkButton(KeyEvent event) {
        dialogBox.setOkButtonEnabled(true);
        if (event.getKeyChar() == KeyEvent.VK_ENTER) {
            dialogBox.clickOkButton();
        }
    }

    /** @return A dialog box in which a user can add or modify an entry. */
    EntryDialogBox getDialogBox() {
        return dialogBox;
    }
}
