package dev.bjdelacruz.jlpt.client.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyListener;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.jlpt.common.ui.UiUtils;
import dev.bjdelacruz.commons.ui.WindowCloseAction;

/**
 * A dialog box in which users can add or modify an entry. An entry consists of a Japanese word, its
 * reading, and its meaning in English.
 * 
 * @author BJ Peter DeLaCruz
 */
final class EntryDialogBox extends JFrame {

  private final DbManager databaseManager;
  private final ClientMain client;
  private final JTextField japaneseWordTextField;
  private final JTextField readingTextField;
  private final JTextField engTextField;
  private final JButton okButton;

  /**
   * Creates a new EntryDialogBox. All the UI components are added here.
   * 
   * @param databaseManager The database manager.
   * @param client The client application.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  EntryDialogBox(DbManager databaseManager, ClientMain client) {
    if (databaseManager == null) {
      throw new IllegalArgumentException("Database manager must not be null.");
    }
    if (client == null) {
      throw new IllegalArgumentException("Client must not be null.");
    }
    this.databaseManager = databaseManager;
    this.client = client;

    setIconImage(UiUtils.getJapaneseFlagIcon());
    setLayout(new BorderLayout());

    var grid = new GridBagLayout();
    var constraints = new GridBagConstraints();
    constraints.anchor = GridBagConstraints.EAST;

    var panel = new JPanel(grid);

    var japaneseWordLabel = new JLabel("Kana/Kanji: ");
    grid.setConstraints(japaneseWordLabel, constraints);
    panel.add(japaneseWordLabel);

    japaneseWordTextField = new JTextField();
    var defaultHeight = japaneseWordTextField.getPreferredSize().height;
    japaneseWordTextField.setPreferredSize(new Dimension(250, defaultHeight));
    constraints.gridwidth = GridBagConstraints.REMAINDER;
    var textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 2));
    textPanel.add(japaneseWordTextField);
    grid.setConstraints(textPanel, constraints);
    panel.add(textPanel);

    var readingLabel = new JLabel(LangUtils.getString("reading") + ": ");
    constraints.weightx = 0.0;
    constraints.gridwidth = 1;
    grid.setConstraints(readingLabel, constraints);
    panel.add(readingLabel);

    readingTextField = new JTextField();
    readingTextField.setPreferredSize(new Dimension(250, defaultHeight));
    constraints.gridwidth = GridBagConstraints.REMAINDER;
    textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 2));
    textPanel.add(readingTextField);
    grid.setConstraints(textPanel, constraints);
    panel.add(textPanel);

    var engLabel = new JLabel(LangUtils.getString("english_definition") + ": ");
    constraints.weightx = 0.0;
    constraints.gridwidth = 1;
    grid.setConstraints(engLabel, constraints);
    panel.add(engLabel);

    engTextField = new JTextField();
    engTextField.setPreferredSize(new Dimension(250, defaultHeight));
    constraints.gridwidth = GridBagConstraints.REMAINDER;
    textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 2));
    textPanel.add(engTextField);
    grid.setConstraints(textPanel, constraints);
    panel.add(textPanel);

    add(panel, BorderLayout.CENTER);

    var buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    okButton = new JButton();
    okButton.setEnabled(false);
    var cancelButton = new JButton(LangUtils.getString("cancel"));
    cancelButton.addActionListener(new WindowCloseAction(this, LangUtils.getString("exit")));
    cancelButton.setPreferredSize(new Dimension(100, cancelButton.getPreferredSize().height));
    okButton.setPreferredSize(cancelButton.getPreferredSize());
    buttonPanel.add(okButton);
    buttonPanel.add(cancelButton);
    add(buttonPanel, BorderLayout.SOUTH);

    setSize(400, 160);
    setResizable(false);
    dev.bjdelacruz.commons.utils.UiUtils.setEscKey(this);
  }

  /**
   * Sets the values of the text fields.
   * 
   * @param entry The entry that contains the values used to populate the text fields.
   */
  public void setTextFields(JapaneseEntry entry) {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    japaneseWordTextField.setText(entry.japanese());
    readingTextField.setText(entry.reading());
    engTextField.setText(entry.english());
  }

  /**
   * Sets the key listener for all three text fields.
   * 
   * @param listener The listener for the three text fields.
   */
  public void setKeyListener(KeyListener listener) {
    if (listener == null) {
      throw new IllegalArgumentException("Listener must not be null.");
    }
    Arrays.stream(japaneseWordTextField.getKeyListeners()).forEach(japaneseWordTextField::removeKeyListener);
    japaneseWordTextField.addKeyListener(listener);

    Arrays.stream(readingTextField.getKeyListeners()).forEach(readingTextField::removeKeyListener);
    readingTextField.addKeyListener(listener);

    Arrays.stream(engTextField.getKeyListeners()).forEach(engTextField::removeKeyListener);
    engTextField.addKeyListener(listener);
  }

  /**
   * Removes the previous actions and then sets the given action for the OK button.
   * 
   * @param action The action for the OK button.
   */
  public void setOkButtonAction(AbstractAction action) {
    if (action == null) {
      throw new IllegalArgumentException("Action must not be null.");
    }
    Arrays.stream(okButton.getActionListeners()).forEach(okButton::removeActionListener);
    okButton.addActionListener(action);
  }

  /**
   * Sets the text for the OK button.
   * 
   * @param text The text for the OK button.
   */
  public void setOkButtonText(String text) {
    if (text == null || text.isBlank()) {
      throw new IllegalArgumentException("Text must not be null or an empty string.");
    }
    okButton.setText(text);
  }

  /** @return An entry with the updated values from the text fields. */
  public JapaneseEntry getUpdatedEntry() {
    return new JapaneseEntry(japaneseWordTextField.getText(), readingTextField.getText(),
        engTextField.getText());
  }

  /** @return The Japanese word that was entered in the Kana/Kanji text field. */
  public String getJapaneseWordText() {
    return japaneseWordTextField.getText();
  }

  /** @return The reading that was entered in the Reading text field. */
  public String getReadingText() {
    return readingTextField.getText();
  }

  /** @return The English meaning that was entered in the English Meaning text field. */
  public String getEnglishTranslationText() {
    return engTextField.getText();
  }

  /** @return The database manager. */
  public DbManager getDbManager() {
    return databaseManager;
  }

  /** @return The main client frame. */
  public ClientMain getClientMainFrame() {
    return client;
  }

  /** @param enabled True to enable the OK button, false otherwise. */
  public void setOkButtonEnabled(boolean enabled) {
    okButton.setEnabled(enabled);
  }

  /** Performs a click on the OK button. */
  void clickOkButton() {
    if (okButton.isEnabled()) {
      okButton.doClick();
    }
  }

}
