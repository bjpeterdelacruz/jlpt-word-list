package dev.bjdelacruz.jlpt.client.main;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import dev.bjdelacruz.jlpt.common.db.EntryAlreadyExistsException;
import dev.bjdelacruz.commons.utils.UiUtils;

/**
 * An action that will add an entry to the database when the user clicks on the OK button.
 * 
 * @author BJ Peter DeLaCruz
 */
final class AddEntryAction extends AbstractAction {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final EntryDialogBox dialogBox;

  /**
   * Creates a new AddEntryAction.
   * 
   * @param dialogBox The dialog box to which to add this action.
   */
  AddEntryAction(EntryDialogBox dialogBox) {
    Objects.requireNonNull(dialogBox);
    this.dialogBox = dialogBox;
  }

  /** {@inheritDoc} */
  @Override
  public void actionPerformed(ActionEvent event) {
    try {
      dialogBox.getDbManager().addEntry(dialogBox.getUpdatedEntry());
    }
    catch (EntryAlreadyExistsException | IOException | SQLException e) {
      displayErrorMessage(e, dialogBox);
      return;
    }
    dialogBox.getClientMainFrame().updateTable();
    UiUtils.closeFrame(dialogBox);
  }

  /**
   * Displays an error message on the screen.
   * 
   * @param e The exception that was thrown.
   * @param dialogBox The Add New Entry dialog box.
   */
  private static void displayErrorMessage(Exception e, EntryDialogBox dialogBox) {
    LOGGER.log(Level.SEVERE, e.getMessage());
    var msg = "Unable to add entry to the database. Reason:\n\n" + e;
    JOptionPane.showMessageDialog(dialogBox, msg, "Error", JOptionPane.ERROR_MESSAGE);
  }

}
