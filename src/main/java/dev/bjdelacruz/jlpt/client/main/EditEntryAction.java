package dev.bjdelacruz.jlpt.client.main;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import dev.bjdelacruz.jlpt.client.table.EntriesTable;
import dev.bjdelacruz.jlpt.common.db.EntryDoesNotExistException;
import dev.bjdelacruz.jlpt.common.db.StaleEntryException;

/**
 * An action that will update an already existing entry in the database when the user clicks on the
 * OK button.
 * 
 * @author BJ Peter DeLaCruz
 */
final class EditEntryAction extends AbstractAction {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final EntryDialogBox dialogBox;
  private final EntriesTable table;

  /**
   * Creates a new EditEntryAction.
   * 
   * @param dialogBox The dialog box to which to add this action.
   * @param table The table that contains all the entries.
   */
  EditEntryAction(EntryDialogBox dialogBox, EntriesTable table) {
    if (dialogBox == null) {
      throw new IllegalArgumentException("Dialog box must not be null.");
    }
    if (table == null) {
      throw new IllegalArgumentException("Table must not be null.");
    }
    this.dialogBox = dialogBox;
    this.table = table;
  }

  /** {@inheritDoc} */
  @Override
  public void actionPerformed(ActionEvent event) {
    var oldEntry = table.getEntry(dialogBox.getJapaneseWordText());
    try {
      dialogBox.getDbManager().updateEntry(dialogBox.getUpdatedEntry(), oldEntry);
    }
    catch (EntryDoesNotExistException | IOException | StaleEntryException | SQLException e) {
      displayErrorMessage(e, dialogBox);
      return;
    }
    dialogBox.getClientMainFrame().updateTable();
    var windowClosing = new WindowEvent(dialogBox, WindowEvent.WINDOW_CLOSING);
    dialogBox.dispatchEvent(windowClosing);
  }

  /**
   * Displays an error message on the screen.
   * 
   * @param e The exception that was thrown.
   * @param dialogBox The Edit Selected Entry dialog box.
   */
  private static void displayErrorMessage(Exception e, EntryDialogBox dialogBox) {
    LOGGER.log(Level.SEVERE, e.getMessage());
    var msg = "Unable to update entry in the database. Reason:\n\n" + e;
    JOptionPane.showMessageDialog(dialogBox, msg, "Error", JOptionPane.ERROR_MESSAGE);
  }

}
