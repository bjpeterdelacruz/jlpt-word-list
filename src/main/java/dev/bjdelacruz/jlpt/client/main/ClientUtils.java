package dev.bjdelacruz.jlpt.client.main;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.db.EntryDoesNotExistException;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.jlpt.common.ui.UiUtils;

/**
 * Utility methods for the client UI.
 * 
 * @author BJ Peter DeLaCruz
 */
final class ClientUtils {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /** Do not instantiate this class. */
  private ClientUtils() {
    // Empty constructor.
  }

  /**
   * Displays the Add New Entry dialog box.
   * 
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   */
  private static void displayAddEntryDialogBox(DbManager databaseManager, ClientMain clientMain) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);

    var addEntryDialogBox = new EntryDialogBox(databaseManager, clientMain);
    addEntryDialogBox.setTitle(LangUtils.getString("add_new_entry"));
    addEntryDialogBox.setOkButtonAction(new AddEntryAction(addEntryDialogBox));
    addEntryDialogBox.setOkButtonText(LangUtils.getString("add"));
    addEntryDialogBox.setKeyListener(new AddKeyListener(addEntryDialogBox));
    UiUtils.centerComponentOnParent(clientMain.getClientMainFrame(), addEntryDialogBox);
    addEntryDialogBox.setVisible(true);
  }

  /**
   * Displays the Edit Entry dialog box.
   * 
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   * @param entry The entry to be updated.
   */
  private static void displayEditEntryDialogBox(DbManager databaseManager, ClientMain clientMain, JapaneseEntry entry) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);
    Objects.requireNonNull(entry);

    var editEntryDialogBox = new EntryDialogBox(databaseManager, clientMain);
    editEntryDialogBox.setTitle(LangUtils.getString("edit_entry"));
    editEntryDialogBox.setTextFields(entry);
    editEntryDialogBox.setOkButtonAction(new EditEntryAction(editEntryDialogBox, clientMain.getTable()));
    editEntryDialogBox.setOkButtonText(LangUtils.getString("update"));
    editEntryDialogBox.setKeyListener(new EditKeyListener(editEntryDialogBox, entry));
    UiUtils.centerComponentOnParent(clientMain.getClientMainFrame(), editEntryDialogBox);
    editEntryDialogBox.setVisible(true);
  }

  /**
   * Returns the action to refresh the table.
   * 
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   * @return The action to refresh the table.
   */
  public static ActionListener getRefreshTableAction(DbManager databaseManager, ClientMain clientMain) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);

    return _ -> {
        try {
          clientMain.updateTable(databaseManager.getEntries());
        }
        catch (Exception e) {
          LOGGER.log(Level.SEVERE, e.getMessage());
        }
    };
  }

  /**
   * Returns the action to add a new entry to the database.
   * 
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   * @return The action to add a new entry to the database.
   */
  public static ActionListener getAddNewEntryAction(DbManager databaseManager, ClientMain clientMain) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);

    return _ -> displayAddEntryDialogBox(databaseManager, clientMain);
  }

  /**
   * Returns the action to edit the selected entry.
   * 
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   * @return The action to edit the selected entry.
   */
  public static ActionListener getEditSelectedEntryAction(DbManager databaseManager, ClientMain clientMain) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);

    return _ -> displayEditEntryDialogBox(databaseManager, clientMain, clientMain.getSelectedEntry());
  }

  /**
   * Returns the action to remove the selected entry from the database.
   * 
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   * @return The action to remove the selected entry from the database.
   */
  public static ActionListener getRemoveSelectedEntryAction(DbManager databaseManager, ClientMain clientMain) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);

    return _ -> displayRemoveEntryConfirmDialogBox(databaseManager, clientMain, clientMain.getSelectedEntry());
  }

  /**
   * Displays the Remove Entry confirm dialog box.
   *
   * @param databaseManager The database manager.
   * @param clientMain The client application.
   * @param selectedEntry The selected entry.
   */
  private static void displayRemoveEntryConfirmDialogBox(DbManager databaseManager, ClientMain clientMain, JapaneseEntry selectedEntry) {
    Objects.requireNonNull(databaseManager);
    Objects.requireNonNull(clientMain);
    Objects.requireNonNull(selectedEntry);

    var msg = "Are you sure you want to delete the following entry from the database?\n\n";
    msg += selectedEntry.getEntryAsString(" : ") + "\n\n";
    var mainFrame = clientMain.getClientMainFrame();
    var option = JOptionPane.showConfirmDialog(mainFrame, msg, "Delete Selected Entry", JOptionPane.YES_NO_OPTION);
    if (option == JOptionPane.NO_OPTION) {
      return;
    }
    try {
      databaseManager.removeEntry(selectedEntry);
    }
    catch (EntryDoesNotExistException | IOException | SQLException e) {
      displayErrorMessage(e, selectedEntry, mainFrame);
      return;
    }
    msg = "   Successfully removed " + selectedEntry.japanese() + " from the database.";
    clientMain.getStatusLabel().setText(msg);
    clientMain.updateTable();
  }

  /**
   * Displays an error message on the screen.
   * 
   * @param e The exception that was thrown.
   * @param entry The selected entry.
   * @param frame The main frame of this application.
   */
  private static void displayErrorMessage(Exception e, JapaneseEntry entry, JFrame frame) {
    var msg = "Unable to remove selected entry from database: " + entry + "\n";
    LOGGER.log(Level.SEVERE, msg + e.getMessage());
    JOptionPane.showMessageDialog(frame, msg, "Error", JOptionPane.ERROR_MESSAGE);
  }

}
