package dev.bjdelacruz.jlpt.client.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.MouseInputAdapter;

import dev.bjdelacruz.commons.ui.AboutDialogConfig;
import dev.bjdelacruz.commons.ui.AppAboutDialog;
import dev.bjdelacruz.commons.ui.AppStatusBar;
import dev.bjdelacruz.commons.ui.WindowCloseAction;
import dev.bjdelacruz.jlpt.client.table.CopyToClipboardAction;
import dev.bjdelacruz.jlpt.client.table.EntriesTable;
import dev.bjdelacruz.jlpt.client.table.EntriesTableModel;
import dev.bjdelacruz.jlpt.client.table.ExportAction;
import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.jlpt.common.ui.UiUtils;
import dev.bjdelacruz.jlpt.common.utils.PropertiesFileUtils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * The main UI for the client.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class ClientMain implements ServerStatusListener {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final DbManager databaseManager;
  private final EntriesTable table;
  private final JFrame frame;
  private JButton editButton;
  private JButton removeButton;
  private JapaneseEntry selectedEntry;
  private JLabel statusLabel;
  private JLabel serverStatusLabel;
  private JMenuItem editSelectedEntryMenuItem;
  private JMenuItem removeSelectedEntryMenuItem;
  private JTextField searchField;
  private JButton clearResultsButton;
  private final DateFormat dateFormat;
  private static final String EI_EXPOSE_REP = "EI_EXPOSE_REP";

  /**
   * Creates a new ClientMain instance. Creates the main client window and displays it on the user's
   * screen.
   * 
   * @param databaseManager The database manager.
   * @param entries The list of entries used to populate the database.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public ClientMain(DbManager databaseManager, List<JapaneseEntry> entries) {
    if (databaseManager == null) {
      throw new IllegalArgumentException("Database manager must not be null.");
    }
    if (entries == null) {
      throw new IllegalArgumentException("Entries must not be null.");
    }
    this.databaseManager = databaseManager;
    dateFormat = new SimpleDateFormat("MMMM dd, yyyy 'at' HH:mm:ss z", Locale.US);

    table = new EntriesTable(new EntriesTableModel(entries));

    addPopupMenuItems();

    table.addMouseListener(new MouseInputAdapter() {

      @Override
      public void mouseClicked(MouseEvent event) {
        editButton.setEnabled(true);
        removeButton.setEnabled(true);
        editSelectedEntryMenuItem.setEnabled(true);
        removeSelectedEntryMenuItem.setEnabled(true);
        int row = table.rowAtPoint(new Point(event.getX(), event.getY()));
        selectedEntry = table.getEntry(row);
      }

    });

    frame = new JFrame();

    frame.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent event) {
        if (databaseManager instanceof ClientDbManager dbManager) {
          try {
            dbManager.quit();
          }
          catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
          }
        }
      }

    });

    var scrollPane =
        new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.getViewport().setOpaque(false);
    scrollPane.setBackground(Color.WHITE);

    var menuBar = new JMenuBar();
    addMenuItems(frame, menuBar, table);

    var buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    addButtons(buttonPanel);

    var searchPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    addSearchFields(searchPanel);

    var panel = new JPanel(new BorderLayout());
    panel.add(buttonPanel, BorderLayout.NORTH);
    panel.add(scrollPane, BorderLayout.CENTER);
    panel.add(searchPanel, BorderLayout.SOUTH);

    setProperties(frame);

    frame.add(panel, BorderLayout.CENTER);
    addStatusBar();

    frame.setJMenuBar(menuBar);
    updateStatusLabel(entries);
    // frame.pack();
    frame.setVisible(true);
  }

  /**
   * Adds menu items to the right-click popup menu.
   */
  private void addPopupMenuItems() {
    var refreshMenuItem = new JMenuItem(LangUtils.getString("refresh_table"));
    refreshMenuItem
        .addActionListener(ClientUtils.getRefreshTableAction(databaseManager, this));
    var menu = table.getPopupMenu();
    menu.add(refreshMenuItem);
    menu.add(new JSeparator());

    var addMenuItem = new JMenuItem(LangUtils.getString("add_new_entry"));
    addMenuItem.addActionListener(ClientUtils.getAddNewEntryAction(databaseManager, this));
    menu.add(addMenuItem);

    var editMenuItem = new JMenuItem(LangUtils.getString("edit_selected"));
    editMenuItem.addActionListener(ClientUtils.getEditSelectedEntryAction(databaseManager,
        this));
    menu.add(editMenuItem);

    var removeMenuItem = new JMenuItem(LangUtils.getString("delete_selected"));
    removeMenuItem.addActionListener(ClientUtils.getRemoveSelectedEntryAction(databaseManager,
        this));
    menu.add(removeMenuItem);
    menu.add(new JSeparator());
    menu.add(new ExportAction(table, menu));
    menu.add(new CopyToClipboardAction(table));
  }

  /**
   * Updates the table with the most recent entries in the database.
   */
  void updateTable() {
    try {
      updateTable(databaseManager.getEntries());
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
    }
  }

  /**
   * Updates the table with a list of entries.
   * 
   * @param entries The list of entries used to populate the table.
   */
  void updateTable(List<JapaneseEntry> entries) {
    if (entries == null) {
      throw new IllegalArgumentException("Entries must not be null.");
    }
    table.setModel(new EntriesTableModel(entries));

    selectedEntry = table.selectEntry(selectedEntry);
    updateStatusLabel(entries);

    frame.invalidate();
    frame.validate();
    frame.repaint();
  }

  private void updateStatusLabel(List<JapaneseEntry> entries) {
    var msg = "   Found " + (entries.size() == 1 ? "1 entry. " : entries.size() + " entries. ");
    statusLabel.setText(msg + "Last updated on " + dateFormat.format(new Date()) + ".");
  }

  /**
   * Sets the properties for the client frame.
   * 
   * @param frame The client frame.
   */
  private void setProperties(JFrame frame) {
    if (frame == null) {
      throw new IllegalArgumentException("Frame not be null.");
    }
    UiUtils.setFrameProperties(frame, LangUtils.getString("client_title"));

    // Make the frame half the height and width of the monitor.
    var screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    var height = screenSize.height;
    var width = screenSize.width;
    frame.setSize(width / 2, height / 2);
    frame.setResizable(false);

    // Center on screen.
    frame.setLocationRelativeTo(null);
    dev.bjdelacruz.commons.utils.UiUtils.setEscKey(frame);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  /**
   * Adds buttons to the panel above the table.
   *
   * @param buttonPanel The panel to which to add buttons.
   */
  private void addButtons(JPanel buttonPanel) {
    if (buttonPanel == null) {
      throw new IllegalArgumentException("Button panel must not be null.");
    }
    var refreshButton = new JButton(LangUtils.getString("refresh_table"));
    refreshButton.setMnemonic(KeyEvent.VK_R);
    refreshButton.addActionListener(ClientUtils.getRefreshTableAction(databaseManager, this));
    buttonPanel.add(refreshButton);

    var addButton = new JButton(LangUtils.getString("add_new_entry"));
    addButton.setMnemonic(KeyEvent.VK_A);
    addButton.addActionListener(ClientUtils.getAddNewEntryAction(databaseManager, this));
    buttonPanel.add(addButton);

    editButton = new JButton(LangUtils.getString("edit_selected"));
    editButton.setMnemonic(KeyEvent.VK_E);
    editButton.addActionListener(ClientUtils.getEditSelectedEntryAction(databaseManager,
        this));
    buttonPanel.add(editButton);

    removeButton = new JButton(LangUtils.getString("delete_selected"));
    removeButton.setMnemonic(KeyEvent.VK_D);
    removeButton.addActionListener(ClientUtils.getRemoveSelectedEntryAction(
        databaseManager, this));
    buttonPanel.add(removeButton);

    editButton.setEnabled(false);
    removeButton.setEnabled(false);

    Dimension preferredSize;
    if ("es".equals(Locale.getDefault().getLanguage())) {
      preferredSize = addButton.getPreferredSize();
      removeButton.setPreferredSize(preferredSize);
    }
    else {
      preferredSize = removeButton.getPreferredSize();
      addButton.setPreferredSize(preferredSize);
    }
    refreshButton.setPreferredSize(preferredSize);
    editButton.setPreferredSize(preferredSize);
  }

  /**
   * Adds a text field and button to the search panel.
   * 
   * @param searchPanel The search panel to which to add a text field and button.
   */
  private void addSearchFields(JPanel searchPanel) {
    if (searchPanel == null) {
      throw new IllegalArgumentException("Search panel must not be null.");
    }
    final var searchButton = new JButton(LangUtils.getString("search"));
    searchButton.setMnemonic(KeyEvent.VK_S);
    searchButton.setEnabled(false);

    searchField = new JTextField();
    var msg = """
            <html>Examples of valid patterns for the search text field:<pre>  thank</pre><pre>  [ab]+out</pre>
            <pre>  ^&#x3042</pre></html>""";
    searchField.setToolTipText(msg);
    searchField.addKeyListener(new SearchFieldKeyListener(searchButton));
    var dimension = new Dimension(250, searchField.getPreferredSize().height);
    searchField.setPreferredSize(dimension);
    searchPanel.add(searchField);

    clearResultsButton = new JButton(LangUtils.getString("clear_results"));
    clearResultsButton.setMnemonic(KeyEvent.VK_C);
    clearResultsButton.addActionListener(_ -> clearResults());
    clearResultsButton.setEnabled(false);

    searchButton.addActionListener(_ -> executeSearch());
    searchPanel.add(searchButton);
    searchPanel.add(clearResultsButton);
  }

  /**
   * A key listener for the search field that will either execute a search using the given regular
   * expression pattern or clear the results from the screen when the Enter key is pressed and
   * depending on the status of the Search and Clear Results buttons.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class SearchFieldKeyListener extends KeyAdapter {

    private final JButton searchButton;

    /**
     * Creates a new SearchFieldKeyListener.
     * 
     * @param searchButton The Search button.
     */
    SearchFieldKeyListener(JButton searchButton) {
      Objects.requireNonNull(searchButton);
      this.searchButton = searchButton;
    }

    /** {@inheritDoc} */
    @Override
    public void keyReleased(KeyEvent event) {
      searchButton.setEnabled(!searchField.getText().isEmpty());
      if (event.getKeyChar() == KeyEvent.VK_ENTER) {
        if (searchButton.isEnabled()) {
          executeSearch();
        }
        else if (clearResultsButton.isEnabled()) {
          clearResults();
        }
      }
    }
  }

  /**
   * Clears the results in the table.
   */
  private void clearResults() {
    statusLabel.setText("");
    searchField.setText("");
    updateTable();
    clearResultsButton.setEnabled(false);
  }

  /**
   * Executes a search against the database.
   */
  private void executeSearch() {
    try {
      updateTable(databaseManager.find(searchField.getText()));
      clearResultsButton.setEnabled(true);
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage());

      var msg = "An error was encountered while trying to find entries in the database: \n\n" + e.getMessage() + "\n\n";
      JOptionPane.showMessageDialog(frame, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Adds menus and menu items to the menu bar.
   * 
   * @param frame The frame for the client application.
   * @param menuBar The menu bar to which to add menus.
   * @param table The table that contains Japanese words and their English meanings.
   */
  private void addMenuItems(JFrame frame, JMenuBar menuBar, JTable table) {
    if (frame == null) {
      throw new IllegalArgumentException("Frame must not be null.");
    }
    if (menuBar == null) {
      throw new IllegalArgumentException("Menu bar must not be null.");
    }
    if (table == null) {
      throw new IllegalArgumentException("Table must not be null.");
    }
    var optionsMenu = new JMenu(LangUtils.getString("options"));
    var refreshMenuItem = new JMenuItem(LangUtils.getString("refresh_table"));
    refreshMenuItem.addActionListener(ClientUtils.getRefreshTableAction(databaseManager, this));
    optionsMenu.add(refreshMenuItem);
    optionsMenu.add(new JSeparator());
    optionsMenu.setMnemonic(KeyEvent.VK_O);

    var addEntryMenuItem = new JMenuItem(LangUtils.getString("add_new_entry"));
    addEntryMenuItem.addActionListener(ClientUtils.getAddNewEntryAction(databaseManager, this));
    optionsMenu.add(addEntryMenuItem);

    editSelectedEntryMenuItem = new JMenuItem(LangUtils.getString("edit_selected"));
    editSelectedEntryMenuItem.addActionListener(ClientUtils.getEditSelectedEntryAction(
        databaseManager, this));
    editSelectedEntryMenuItem.setEnabled(false);
    optionsMenu.add(editSelectedEntryMenuItem);

    removeSelectedEntryMenuItem = new JMenuItem(LangUtils.getString("delete_selected"));
    removeSelectedEntryMenuItem.addActionListener(ClientUtils.getRemoveSelectedEntryAction(
        databaseManager, this));
    removeSelectedEntryMenuItem.setEnabled(false);
    optionsMenu.add(removeSelectedEntryMenuItem);

    optionsMenu.add(new JSeparator());
    optionsMenu.add(new ExportAction(table, optionsMenu));
    optionsMenu.add(new CopyToClipboardAction(table));
    optionsMenu.add(new JSeparator());
    optionsMenu.add(new WindowCloseAction(frame, LangUtils.getString("exit")));
    menuBar.add(optionsMenu);

    var helpMenu = new JMenu(LangUtils.getString("help"));
    var aboutMenuItem = new JMenuItem(LangUtils.getString("about"));
    aboutMenuItem.addActionListener(_ -> {
      var config = new AboutDialogConfig.AboutDialogConfigBuilder()
              .title(PropertiesFileUtils.getValue("application.name"))
              .version(PropertiesFileUtils.getValue("version"))
              .copyright(PropertiesFileUtils.getValue("copyright"))
              .website(PropertiesFileUtils.getValue("author.website"))
              .email(PropertiesFileUtils.getValue("author.email")).build();
      new AppAboutDialog(config, UiUtils.getAboutIcon(), UiUtils.getJapaneseFlagIcon(), frame).setVisible(true);
    });
    helpMenu.add(aboutMenuItem);
    var userGuideMenuItem = new JMenuItem(LangUtils.getString("user_guide"));
    userGuideMenuItem.addActionListener(_ -> {
        try {
          var desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
          if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            desktop.browse(URI.create("https://bjdelacruz.dev"));
          }
        }
        catch (IOException e) {
          LOGGER.log(Level.SEVERE, String.format("Unable to navigate to developer's website: %s", e.getMessage()), e);
        }
    });
    // helpMenu.add(userGuideMenuItem);
    helpMenu.setMnemonic(KeyEvent.VK_H);
    menuBar.add(helpMenu);
  }

  /**
   * Adds a status bar to the given frame.
   */
  private void addStatusBar() {
    var statusBar = new AppStatusBar();

    var eastPanel = new JPanel();
    eastPanel.setOpaque(false);
    statusLabel = new JLabel("");
    eastPanel.add(statusLabel);

    var westPanel = new JPanel();
    westPanel.setOpaque(false);
    var padding = new JLabel();
    serverStatusLabel = new JLabel("");
    westPanel.add(padding);
    westPanel.add(serverStatusLabel);

    statusBar.addComponent(eastPanel, BorderLayout.EAST);
    statusBar.addComponent(westPanel, BorderLayout.WEST);

    frame.add(statusBar, BorderLayout.SOUTH);
  }

  /** @return The frame for the client application. */
  @SuppressFBWarnings(EI_EXPOSE_REP)
  public JFrame getClientMainFrame() {
    return frame;
  }

  /** @return The table that contains all the entries. */
  @SuppressFBWarnings(EI_EXPOSE_REP)
  public EntriesTable getTable() {
    return table;
  }

  /** @return The status label. */
  @SuppressFBWarnings(EI_EXPOSE_REP)
  public JLabel getStatusLabel() {
    return statusLabel;
  }

  /** @return The selected entry in the table. */
  @SuppressFBWarnings(EI_EXPOSE_REP)
  public JapaneseEntry getSelectedEntry() {
    return selectedEntry;
  }

  /** Sets the icon and text for the server status label to standalone mode. */
  public void setStandaloneStatus() {
    serverStatusLabel.setText(LangUtils.getString("running_in_standalone_mode"));
    serverStatusLabel.setIcon(UiUtils.getStandaloneModeIcon());
  }

  /** {@inheritDoc} */
  @Override
  public void serverStatusChanged(ServerStatus status) {
    if (status == null) {
      throw new IllegalArgumentException("Status must not be null.");
    }
    switch (status) {
      case ONLINE -> {
        var connectedMsg = LangUtils.getString("connected_to_server");
        if (databaseManager instanceof ClientDbManager dbManager) {
          connectedMsg += " " + dbManager.getServerAddress();
        }
        serverStatusLabel.setText(connectedMsg);
        serverStatusLabel.setIcon(UiUtils.getOnlineIcon());
      }
      case OFFLINE -> {
        serverStatusLabel.setText(LangUtils.getString("not_connected_to_server"));
        serverStatusLabel.setIcon(UiUtils.getOfflineIcon());
        JOptionPane.showMessageDialog(frame, "The connection to the server has been lost.", "Error",
                JOptionPane.ERROR_MESSAGE);
      }
      default -> throw new IllegalArgumentException("Unsupported server status: " + status);
    }
  }

}
