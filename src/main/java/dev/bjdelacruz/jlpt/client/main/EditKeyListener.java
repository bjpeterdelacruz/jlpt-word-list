package dev.bjdelacruz.jlpt.client.main;

import java.awt.event.KeyEvent;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * An action that will enable the OK button once the user updates the text in any one of the three
 * text fields.
 * 
 * @author BJ Peter DeLaCruz
 */
final class EditKeyListener extends EntryDialogBoxKeyAdapter {

  private final JapaneseEntry oldEntry;

  /**
   * Creates a new EditKeyListener.
   * 
   * @param dialogBox The dialog box that contains the text fields that are using this listener.
   * @param oldEntry The old entry.
   */
  EditKeyListener(EntryDialogBox dialogBox, JapaneseEntry oldEntry) {
    super(dialogBox);
    if (oldEntry == null) {
      throw new IllegalArgumentException("Old entry must not be null.");
    }
    this.oldEntry = oldEntry;
  }

  /** {@inheritDoc} */
  @Override
  public void keyReleased(KeyEvent event) {
    if (disabledOkButton()) {
      return;
    }
    var japaneseWord = getDialogBox().getJapaneseWordText();
    var reading = getDialogBox().getReadingText();
    var englishTranslation = getDialogBox().getEnglishTranslationText();
    var entry = new JapaneseEntry(japaneseWord, reading, englishTranslation);
    if (entry.equals(oldEntry)) {
      getDialogBox().setOkButtonEnabled(false);
      return;
    }
    enableOkButton(event);
  }

}
