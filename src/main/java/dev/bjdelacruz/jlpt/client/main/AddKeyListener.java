package dev.bjdelacruz.jlpt.client.main;

import java.awt.event.KeyEvent;

/**
 * A key listener that will enable the OK button once the user inputs text in all three text fields.
 * 
 * @author BJ Peter DeLaCruz
 */
class AddKeyListener extends EntryDialogBoxKeyAdapter {

  /**
   * Creates a new AddKeyListener.
   * 
   * @param dialogBox The dialog box that contains the text fields that are using this listener.
   */
  AddKeyListener(EntryDialogBox dialogBox) {
    super(dialogBox);
  }

  /** {@inheritDoc} */
  @Override
  public void keyReleased(KeyEvent event) {
    if (disabledOkButton()) {
      return;
    }
    enableOkButton(event);
  }

}
