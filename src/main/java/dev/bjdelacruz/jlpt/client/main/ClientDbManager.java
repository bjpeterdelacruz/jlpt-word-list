package dev.bjdelacruz.jlpt.client.main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.jlpt.common.db.AddRemoveServerRequest;
import dev.bjdelacruz.jlpt.common.db.Commands;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.db.FindServerRequest;
import dev.bjdelacruz.jlpt.common.db.UpdateServerRequest;

/**
 * A client-side proxy that sends requests to a server using a socket.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class ClientDbManager implements DbManager {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(1);

  private final String hostname;
  private final int port;
  private final Socket socket;
  private final ObjectInputStream inputStream;
  private final ObjectOutputStream outputStream;
  private ServerStatusListener listener;

  /**
   * Creates a new ClientDbManager.
   * 
   * @param hostname The hostname of the server.
   * @param port The server port.
   * @throws IOException If there are problems connecting to the server.
   */
  public ClientDbManager(String hostname, int port) throws IOException {
    if (hostname == null || hostname.isBlank()) {
      throw new IllegalArgumentException("Hostname must not be null or an empty string.");
    }
    if (port < 0 || port > 65_535) {
      throw new IllegalArgumentException("Port number must be between 0 and 65535, inclusive.");
    }

    this.hostname = hostname;
    this.port = port;
    socket = new Socket(hostname, port);
    outputStream = new ObjectOutputStream(socket.getOutputStream());
    outputStream.flush();
    inputStream = new ObjectInputStream(socket.getInputStream());
  }

  /** {@inheritDoc} */
  @Override
  public void addEntry(JapaneseEntry entry) throws IOException {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    var request = new AddRemoveServerRequest(Commands.ADD, entry);
    outputStream.writeObject(request);
  }

  /** {@inheritDoc} */
  @Override
  public void removeEntry(JapaneseEntry entry) throws IOException {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    var request = new AddRemoveServerRequest(Commands.REMOVE, entry);
    outputStream.writeObject(request);
  }

  /** {@inheritDoc} */
  @Override
  public void updateEntry(JapaneseEntry newEntry, JapaneseEntry oldEntry) throws IOException {
    if (newEntry == null) {
      throw new IllegalArgumentException("New entry must not be null.");
    }
    if (oldEntry == null) {
      throw new IllegalArgumentException("Old entry must not be null.");
    }
    var request = new UpdateServerRequest(newEntry, oldEntry);
    outputStream.writeObject(request);
  }

  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  @Override
  public List<JapaneseEntry> find(String regexPattern) throws ClassNotFoundException, IOException {
    if (regexPattern == null) {
      throw new IllegalArgumentException("Regex pattern must not be null.");
    }
    var request = new FindServerRequest(regexPattern);
    outputStream.writeObject(request);
    return (List<JapaneseEntry>) inputStream.readObject();
  }

  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  @Override
  public List<JapaneseEntry> getEntries() throws ClassNotFoundException, IOException {
    outputStream.writeObject(Commands.GET);
    return (List<JapaneseEntry>) inputStream.readObject();
  }

  /** @return The address and port of the remote server. */
  public String getServerAddress() {
    return socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
  }

  /**
   * Tells the server that this client is going to quit.
   * 
   * @throws IOException If there are problems trying to send the QUIT command to the server.
   */
  void quit() throws IOException {
    outputStream.writeObject(Commands.QUIT);
  }

  /**
   * Closes the connection.
   * 
   * @return True if the socket has been closed, false otherwise.
   * @throws IOException If there are problems trying to close the socket.
   */
  boolean close() throws IOException {
    socket.close();
    return socket.isClosed();
  }

  /**
   * Sets the listener for receiving server status events.
   * 
   * @param listener The listener to set.
   */
  public void setServerStatusListener(ServerStatusListener listener) {
    if (listener == null) {
      throw new IllegalArgumentException("Listener must not be null.");
    }
    this.listener = listener;
  }

  /** Starts the task that will check the status of the server periodically. */
  public void startCheckServerStatusTask() {
    threadPool.scheduleAtFixedRate(new CheckServerStatusTask(), 0L, 10L, TimeUnit.SECONDS);
  }

  /**
   * A task that will check the status of the server by trying to open a connection to it.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class CheckServerStatusTask implements Runnable {

    /** {@inheritDoc} */
    @Override
    public void run() {
      //CHECKSTYLE:OFF
      try (var _ = new Socket(hostname, port)) {
      //CHECKSTYLE:ON
        listener.serverStatusChanged(ServerStatus.ONLINE);
      }
      catch (IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage());
        listener.serverStatusChanged(ServerStatus.OFFLINE);
      }
    }

  }

}
