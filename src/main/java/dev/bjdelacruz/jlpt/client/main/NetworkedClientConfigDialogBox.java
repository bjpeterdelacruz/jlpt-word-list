package dev.bjdelacruz.jlpt.client.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import dev.bjdelacruz.commons.ui.AppStatusBar;
import dev.bjdelacruz.jlpt.common.AppConstants;
import dev.bjdelacruz.jlpt.common.db.DbManager;
import dev.bjdelacruz.jlpt.common.i18n.LangUtils;
import dev.bjdelacruz.jlpt.common.ui.PortTextField;
import dev.bjdelacruz.jlpt.common.ui.UiUtils;
import dev.bjdelacruz.jlpt.common.utils.ThreadUtils;
import dev.bjdelacruz.commons.utils.FileUtils;

/**
 * A dialog box in which the user inputs the URL and port of the server to which to connect.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class NetworkedClientConfigDialogBox extends JFrame {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final JTextField serverNameTextField;
  private final JTextField portTextField;
  private final JButton btnConnect;
  private final JButton btnStartClient;
  private final JLabel statusLabel;
  private DbManager clientDbManager;

  /**
   * Creates a new NetworkedClientConfigDialogBox.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public NetworkedClientConfigDialogBox() {
    getContentPane().setLayout(new BorderLayout());
    UiUtils.setFrameProperties(this, LangUtils.getString("connect_to_server"));

    var serverUrlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
    var serverUrlLabel = new JLabel(LangUtils.getString("server_url") + "  ");
    serverUrlPanel.add(serverUrlLabel);

    serverNameTextField = new JTextField();
    serverNameTextField.addKeyListener(new CustomKeyAdapter());
    serverNameTextField.setDisabledTextColor(Color.BLACK);
    serverNameTextField.setColumns(20);
    serverNameTextField.setMaximumSize(new Dimension(50, serverNameTextField.getPreferredSize().height));
    serverUrlPanel.add(serverNameTextField);

    var portPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    var portLabel = new JLabel(LangUtils.getString("server_port"));
    if (Locale.getDefault().getLanguage().equals("en")) {
      portLabel.setPreferredSize(serverUrlLabel.getPreferredSize());
    }
    else {
      serverUrlLabel.setText(LangUtils.getString("server_url"));
      portLabel.setText(portLabel.getText() + "   ");
      serverUrlLabel.setPreferredSize(portLabel.getPreferredSize());
    }
    portPanel.add(portLabel);

    portTextField = new PortTextField(new CustomKeyAdapter());
    portPanel.add(portTextField);

    var buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    btnStartClient = new JButton(LangUtils.getString("start_client"));
    btnStartClient.setMnemonic(KeyEvent.VK_S);
    btnStartClient.setEnabled(false);
    btnStartClient.addActionListener(_ -> startClient());
    buttonPanel.add(btnStartClient);

    btnConnect = new JButton(LangUtils.getString("connect"));
    btnConnect.addActionListener(new ConnectDisconnectAction());
    btnConnect.setMnemonic(KeyEvent.VK_S);
    btnConnect.setEnabled(false);
    btnConnect.setPreferredSize(btnStartClient.getPreferredSize());
    buttonPanel.add(btnConnect);

    var innerPanel = new JPanel(new GridBagLayout());
    var constraints = new GridBagConstraints();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    innerPanel.add(serverUrlPanel, constraints);
    constraints.gridy = 1;
    innerPanel.add(portPanel, constraints);
    constraints.gridy = 2;
    innerPanel.add(buttonPanel, constraints);
    getContentPane().add(innerPanel, BorderLayout.CENTER);

    var westPanel = new JPanel();
    westPanel.setOpaque(false);
    var padding = new JLabel();
    statusLabel = new JLabel();
    westPanel.add(padding);
    westPanel.add(statusLabel);

    setOfflineStatus();
    var statusBar = new AppStatusBar();
    statusBar.addComponent(westPanel, BorderLayout.WEST);
    getContentPane().add(statusBar, BorderLayout.SOUTH);

    setSize(450, 150);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    dev.bjdelacruz.commons.utils.UiUtils.setEscKey(this);
    setLocationRelativeTo(null);
    setVisible(true);
  }

  /**
   * A key adapter that will enable/disable the Start Client button and, depending on the state of
   * the Start Server button, either display the Open File dialog box or start the server.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class CustomKeyAdapter extends KeyAdapter {

    @Override
    public void keyReleased(KeyEvent event) {
      if (event.getKeyChar() == KeyEvent.VK_ENTER && btnStartClient.isEnabled()) {
        startClient();
        return;
      }

      btnConnect.setEnabled(!serverNameTextField.getText().isEmpty() && !portTextField.getText().isEmpty());

      if (event.getKeyChar() == KeyEvent.VK_ENTER && btnConnect.isEnabled()) {
        connectToServer();
      }
    }

  }

  /**
   * Connects to the server and enables the Start Client button when a connection to the server has
   * been established.
   */
  private void connectToServer() {
    setConnectingStatus();
    ThreadUtils.getSharedThreadPool().execute(new ConnectTask());
  }

  /**
   * An action that will either connect to or disconnect from the server.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class ConnectDisconnectAction implements ActionListener {

    /** {@inheritDoc} */
    @Override
    public void actionPerformed(ActionEvent arg0) {
      if (btnConnect.getText().equals(LangUtils.getString("connect"))) {
        connectToServer();
      }
      else if (btnConnect.getText().equals(LangUtils.getString("disconnect"))) {
        try {
          if (((ClientDbManager) clientDbManager).close()) {
            setOfflineStatus();
            LOGGER.log(Level.INFO, "Disconnected from server.");
            return;
          }
          throw new IllegalStateException("The socket should have been closed.");
        }
        catch (IOException e) {
          LOGGER.log(Level.SEVERE, e.getMessage());
        }
      }
      else {
        throw new IllegalArgumentException("Unsupported action: " + btnConnect.getText());
      }
    }

  }

  /**
   * Attempts to connect to the server when the user clicks on the Connect button.
   * 
   * @author BJ Peter DeLaCruz
   */
  private final class ConnectTask implements Runnable {

    /** {@inheritDoc} */
    @Override
    public void run() {
      var serverName = serverNameTextField.getText();
      var port = Integer.parseInt(portTextField.getText());

      try {
        serverNameTextField.setEnabled(false);
        portTextField.setEditable(false);
        btnConnect.setEnabled(false);

        clientDbManager = new ClientDbManager(serverName, port);

        var msg = "Successfully connected to server.\n   Server URL: " + serverName + "\n   Port: " + port;
        LOGGER.log(Level.INFO, msg);
        SwingUtilities.invokeLater(NetworkedClientConfigDialogBox.this::setOnlineStatus);
      }
      catch (IOException e) {
        var errorMsg = "Unable to connect to server.\n   Server URL: " + serverName + "\n   Port: " + port + "\n   "
                + "Reason: " + e;
        LOGGER.log(Level.SEVERE, errorMsg);
        SwingUtilities.invokeLater(() -> {
          btnConnect.setEnabled(true);
          setOfflineStatus();
          var msg = "Unable to connect to server. Reason:\n\n" + e;
          JOptionPane.showMessageDialog(NetworkedClientConfigDialogBox.this, msg, "Error",
              JOptionPane.ERROR_MESSAGE);
        });
      }
    }

  }

  /**
   * Starts the client application.
   */
  private void startClient() {
    setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    var dbManager = (ClientDbManager) clientDbManager;
    try {
      dbManager.setServerStatusListener(new ClientMain(dbManager, dbManager.getEntries()));
    }
    catch (Exception e) {
      var msg = "There was a problem trying to get entries from the database. Reason:\n\n" + e;
      JOptionPane.showMessageDialog(NetworkedClientConfigDialogBox.this, msg, "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    dbManager.startCheckServerStatusTask();
    try {
      FileUtils.saveProperty(AppConstants.NETWORKED_CLIENT_SERVER_LOCATION_KEY, serverNameTextField.getText(), AppConstants.PROPERTIES_FILE);
      FileUtils.saveProperty(AppConstants.NETWORKED_CLIENT_SERVER_PORT_KEY, portTextField.getText(), AppConstants.PROPERTIES_FILE);
    }
    catch (IOException ioe) {
      var msg = "There was a problem trying to save server name and port number to application.properties. Reason:\n\n" + ioe;
      JOptionPane.showMessageDialog(NetworkedClientConfigDialogBox.this, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
    dev.bjdelacruz.commons.utils.UiUtils.closeFrame(NetworkedClientConfigDialogBox.this);
  }

  /**
   * Sets the label to offline status.
   */
  private void setOfflineStatus() {
    statusLabel.setText(LangUtils.getString("not_connected_to_server"));
    statusLabel.setIcon(UiUtils.getOfflineIcon());
    btnConnect.setText(LangUtils.getString("connect"));
    btnStartClient.setEnabled(false);
    serverNameTextField.setEnabled(true);
    portTextField.setEditable(true);
  }

  /**
   * Sets the label to online status.
   */
  private void setOnlineStatus() {
    var connectedMsg = LangUtils.getString("connected_to_server");
    connectedMsg += " " + serverNameTextField.getText() + ":" + portTextField.getText();
    statusLabel.setText(connectedMsg);
    statusLabel.setIcon(UiUtils.getOnlineIcon());
    btnConnect.setText(LangUtils.getString("disconnect"));
    btnConnect.setEnabled(true);
    btnStartClient.setEnabled(true);
    serverNameTextField.setEnabled(false);
    portTextField.setEditable(false);
  }

  /**
   * Sets the label to online status.
   */
  private void setConnectingStatus() {
    statusLabel.setText(LangUtils.getString("connecting_to_server"));
    statusLabel.setIcon(UiUtils.getConnectingIcon());
  }

}
