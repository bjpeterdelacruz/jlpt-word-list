package dev.bjdelacruz.jlpt.common.datamodel;

import java.io.Serializable;

/**
 * Represents a Japanese word, also contains its reading and English definition.
 *
 * @param japanese The Japanese word.
 * @param reading  The Japanese word written in kana.
 * @param english  The English meaning of the Japanese word.
 * @author BJ Peter DeLaCruz
 */
public record JapaneseEntry(String japanese, String reading, String english) implements Serializable {

  /**
   * Creates a new JapaneseEntry instance.
   */
  public JapaneseEntry {
    if (japanese == null) {
      throw new IllegalArgumentException("Japanese word must not be null.");
    }
    if (reading == null) {
      throw new IllegalArgumentException("Reading must not be null.");
    }
    if (english == null) {
      throw new IllegalArgumentException("English word must not be null.");
    }
    japanese = japanese.trim();
    reading = reading.trim();
    english = english.trim();
  }

  public static JapaneseEntry from(JapaneseEntry entry) {
    return new JapaneseEntry(entry.japanese, entry.reading, entry.english);
  }

  /**
   * Returns the entry as a string separated by the given delimiter.
   *
   * @param delimiter The delimiter to separate parts of the entry.
   * @return The entry as a string.
   */
  public String getEntryAsString(String delimiter) {
    if (delimiter == null || delimiter.isBlank()) {
      throw new IllegalArgumentException("Delimiter must not be null or an empty string.");
    }

    return japanese + delimiter + reading + delimiter + english;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return String.join(" ; ", japanese, reading, english);
  }
}
