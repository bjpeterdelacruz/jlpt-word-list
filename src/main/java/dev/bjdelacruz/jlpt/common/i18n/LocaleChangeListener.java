package dev.bjdelacruz.jlpt.common.i18n;

/**
 * A listener that will change the language of the text in the application.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
@FunctionalInterface
public interface LocaleChangeListener {

  /**
   * Called when the user changes the language of the text in the application.
   */
  void localeChanged();

}
