package dev.bjdelacruz.jlpt.common.i18n;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import dev.bjdelacruz.jlpt.common.AppConstants;
import dev.bjdelacruz.commons.utils.FileUtils;

/**
 * A dialog box in which the user can change the language of the text in the application.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class SelectLanguageDialog extends JDialog {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Creates a new dialog box in which the user can change the language of the text in the
   * application.
   * 
   * @param listener The listener that will change the language of the text.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public SelectLanguageDialog(LocaleChangeListener listener) {
    Objects.requireNonNull(listener);
    setTitle(LangUtils.getString("select_language"));
    setModal(true);
    setLayout(new BorderLayout());

    var panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    var label = new JLabel(LangUtils.getString("select_language_label") + " ");
    panel.add(label);

    var languagesCombo = new JComboBox<Locale>();
    languagesCombo.addItem(Locale.US);
    languagesCombo.addItem(Locale.forLanguageTag("es-ES"));
    languagesCombo.setSelectedIndex(0);
    languagesCombo.setRenderer(new ComboBoxRenderer());
    languagesCombo.setPreferredSize(new Dimension(175, languagesCombo.getPreferredSize().height));
    panel.add(languagesCombo);
    add(panel, BorderLayout.CENTER);

    var southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    var okButton = new JButton(LangUtils.getString("ok"));
    okButton.addActionListener(_ -> {
        Locale.setDefault((Locale) Objects.requireNonNull(languagesCombo.getSelectedItem()));
        LOGGER.info("Locale has been changed to " + Locale.getDefault() + ".");
        listener.localeChanged();
        try {
          FileUtils.saveProperty(AppConstants.LOCALE, Locale.getDefault().getDisplayName(), AppConstants.PROPERTIES_FILE);
        }
        catch (IOException e) {
          LOGGER.log(Level.SEVERE, e.getMessage());
        }
        dispose();
    });
    southPanel.add(okButton);

    var cancelButton = new JButton(LangUtils.getString("cancel"));
    cancelButton.addActionListener(_ -> dispose());
    southPanel.add(cancelButton);
    add(southPanel, BorderLayout.SOUTH);

    setMinimumSize(new Dimension(325, 100));
    setResizable(false);
    setLocationRelativeTo(null);
  }

  /**
   * This renderer will display the language and country of each Locale in the combo box.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private static class ComboBoxRenderer extends JLabel implements ListCellRenderer<Locale> {

    /**
     * Creates a new renderer with no transparency.
     */
    @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
    ComboBoxRenderer() {
      setOpaque(true);
    }

    /** {@inheritDoc} */
    @Override
    public Component getListCellRendererComponent(JList<? extends Locale> list, Locale value,
        int index, boolean isSelected, boolean cellHasFocus) {
      setText(value.getDisplayName());
      if (isSelected) {
        setBackground(list.getSelectionBackground());
        setForeground(list.getSelectionForeground());
      }
      else {
        setBackground(list.getBackground());
        setForeground(list.getForeground());
      }
      return this;
    }

  }

}
