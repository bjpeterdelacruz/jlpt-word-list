package dev.bjdelacruz.jlpt.common.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Utility class that contains methods for returning localized strings.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class LangUtils {

  private static final String BASE_NAME = "labels";

  /** Do not instantiate this class. */
  private LangUtils() {
    // Empty constructor.
  }

  private static final ResourceBundle ENGLISH_BUNDLE;

  static {
    ENGLISH_BUNDLE = ResourceBundle.getBundle(BASE_NAME, Locale.US);
  }

  /**
   * Returns a localized string for the given key.
   * 
   * @param key The key in the properties file.
   * @return The localized string for the given key.
   */
  public static String getString(String key) {
    if (key == null || key.isBlank()) {
      throw new IllegalArgumentException("Key must not be null or an empty string.");
    }
    return ENGLISH_BUNDLE.getString(key);
  }

}
