/**
 * Contains utility classes for localizing the application.
 */
package dev.bjdelacruz.jlpt.common.i18n;
