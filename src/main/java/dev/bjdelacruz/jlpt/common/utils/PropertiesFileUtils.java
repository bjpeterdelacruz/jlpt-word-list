package dev.bjdelacruz.jlpt.common.utils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.io.IOException;
import java.util.Properties;

/**
 * Utility methods for interacting with properties files.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class PropertiesFileUtils {

  /** Do not instantiate this class. */
  private PropertiesFileUtils() {
    // Empty constructor
  }

  /**
   * Gets the value from <code>application.properties</code> for the given property.
   * 
   * @param propertyName The name of the property.
   * @return The value for the given property.
   */
  @SuppressFBWarnings("THROWS_METHOD_THROWS_RUNTIMEEXCEPTION")
  public static String getValue(String propertyName) {
    try {
      var props = new Properties();
      props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
      return props.getProperty(propertyName);
    }
    catch (IOException ioe) {
      var message = String.format("An error occurred while trying to get value for %s", propertyName);
      throw new RuntimeException(message, ioe);
    }
  }

}
