package dev.bjdelacruz.jlpt.common.db;

import java.io.Serializable;

/**
 * A request that the client sends to the server.
 * 
 * @author BJ Peter DeLaCruz
 */
@FunctionalInterface
public interface ServerRequest extends Serializable {

  /** @return The command for the server to process. */
  Commands command();

}
