package dev.bjdelacruz.jlpt.common.db;

/**
 * A request for finding one or more entries in the database using a regular expression pattern.
 *
 * @param regex The regular expression pattern.
 * @author BJ Peter DeLaCruz
 */
public record FindServerRequest(String regex) implements ServerRequest {

  /**
   * Creates a new FindRequest.
   */
  public FindServerRequest {
    if (regex == null || regex.isBlank()) {
      throw new IllegalArgumentException("Regex must not be null or an empty string.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Commands command() {
    return Commands.FIND;
  }

}
