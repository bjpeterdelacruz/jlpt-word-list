package dev.bjdelacruz.jlpt.common.db;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import dev.bjdelacruz.jlpt.common.AppConstants;
import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;
import dev.bjdelacruz.commons.utils.FileUtils;

/**
 * Implements the DbManager interface.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class SqliteDbManagerImpl implements DbManager {

  private static final Logger LOGGER = Logger.getLogger("db");
  static {
    var filename = "logs" + File.separator + "db.log";
    FileUtils.configureLogger(LOGGER, AppConstants.LOG_SIZE, AppConstants.LOG_ROTATION_COUNT, filename);
  }

  private final Map<String, JapaneseEntry> entriesMap = Collections.synchronizedMap(new HashMap<>());
  private final Connection connection;

  /* SQL commands */
  private static final String INSERT =
      "INSERT INTO dictionary (kanji, reading, translation) VALUES (?, ?, ?)";
  private static final String DELETE =
      "DELETE FROM dictionary WHERE kanji = ? AND reading = ? AND translation = ?";
  private static final String SELECT_ALL = "SELECT * FROM dictionary";
  private static final int ONLY_ONE_ENTRY_FOUND = 1;

  /**
   * Creates a new SqliteDbManagerImpl instance. Populates the dictionary table with entries read in
   * from a flat file database.
   *
   * @throws ClassNotFoundException If there are problems loading the JDBC driver.
   * @throws SQLException If there are problems creating or populating the database.
   */
  public SqliteDbManagerImpl() throws ClassNotFoundException, SQLException {
    Class.forName("org.sqlite.JDBC");

    connection = DriverManager.getConnection("jdbc:sqlite::resource:dictionary.db");
    LOGGER.log(Level.INFO, "Connection to database established.");
  }

  /** {@inheritDoc} */
  @Override
  public void addEntry(JapaneseEntry entry) throws EntryAlreadyExistsException, SQLException {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    addEntry(entry, connection);
  }

  /**
   * Adds an entry to a hash map and also to the database.
   * 
   * @param entry The entry to add.
   * @param connection The connection to the database.
   * @throws EntryAlreadyExistsException If an entry already exists in the hash map.
   * @throws SQLException If there are problems adding an entry to the database.
   */
  private void addEntry(JapaneseEntry entry, Connection connection) throws EntryAlreadyExistsException, SQLException {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    if (connection == null) {
      throw new IllegalArgumentException("Connection must not be null.");
    }
    synchronized (entriesMap) {
      var oldEntry = entriesMap.get(entry.japanese());
      if (entry.equals(oldEntry)) {
        throw new EntryAlreadyExistsException(entry);
      }

      try (var statement = connection.prepareStatement(INSERT)) {
        statement.setString(1, entry.japanese());
        statement.setString(2, entry.reading());
        statement.setString(3, entry.english().replace("'", "''"));
        statement.executeUpdate();
        var msg = "Successfully added " + entry.japanese() + " to the database. Values: " + entry;
        LOGGER.log(Level.INFO, msg);
      }

      entriesMap.put(entry.japanese(), entry);
    }

  }

  /** {@inheritDoc} */
  @Override
  public void removeEntry(JapaneseEntry entry) throws EntryDoesNotExistException, SQLException {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    synchronized (entriesMap) {
      var oldEntry = entriesMap.get(entry.japanese());
      if (oldEntry == null) {
        throw new EntryDoesNotExistException(entry);
      }

      try (var statement = connection.prepareStatement(DELETE)) {
        statement.setString(1, entry.japanese());
        statement.setString(2, entry.reading());
        statement.setString(3, entry.english().replace("'", "''"));
        statement.executeUpdate();
        String msg = "Successfully removed " + entry.japanese() + " from the database.";
        LOGGER.log(Level.INFO, msg);
      }

      entriesMap.remove(entry.japanese());
    }
  }

  /** {@inheritDoc} */
  @Override
  public void updateEntry(JapaneseEntry newEntry, JapaneseEntry oldEntry)
          throws EntryDoesNotExistException, StaleEntryException, SQLException {
    if (newEntry == null) {
      throw new IllegalArgumentException("New entry must not be null.");
    }
    if (oldEntry == null) {
      throw new IllegalArgumentException("Old entry must not be null.");
    }
    synchronized (entriesMap) {
      var entry = entriesMap.remove(oldEntry.japanese());
      if (entry == null) {
        throw new EntryDoesNotExistException(oldEntry);
      }
      else if (!entry.equals(oldEntry)) {
        throw new StaleEntryException(oldEntry);
      }

      try (var statement = connection.prepareStatement(
              "UPDATE dictionary SET kanji = ?, reading = ?, translation = ? WHERE kanji = ? AND reading = ? AND translation = ?")) {
        statement.setString(1, newEntry.japanese());
        statement.setString(2, newEntry.reading());
        statement.setString(3, newEntry.english().replace("'", "''"));
        statement.setString(4, oldEntry.japanese());
        statement.setString(5, oldEntry.reading());
        statement.setString(6, oldEntry.english().replace("'", "''"));
        statement.executeUpdate();
        var msg = "Successfully updated " + oldEntry.japanese() + " in the database. New values: " + newEntry;
        LOGGER.log(Level.INFO, msg);
      }

      entriesMap.put(entry.japanese(), newEntry);
    }
  }

  /**
   * {@inheritDoc}
   * 
   * @throws InvalidRegExPatternException
   */
  @Override
  @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
  public List<JapaneseEntry> find(String regexPattern) throws InvalidRegExPatternException {
    if (regexPattern == null || regexPattern.isEmpty()) {
      throw new InvalidRegExPatternException("Regular expression pattern must not be null or an empty string.");
    }

    var results = new ArrayList<JapaneseEntry>();
    synchronized (entriesMap) {
      try {
        var pattern = Pattern.compile(regexPattern);
        for (var entries : entriesMap.entrySet()) {
          var entry = entries.getValue();
          var matcher = pattern.matcher(entry.japanese());
          if (matcher.find()) {
            results.add(entry);
            continue;
          }
          matcher = pattern.matcher(entry.reading());
          if (matcher.find()) {
            results.add(entry);
            continue;
          }
          matcher = pattern.matcher(entry.english());
          if (matcher.find()) {
            results.add(entry);
          }
        }
      }
      catch (PatternSyntaxException exception) {
        throw new InvalidRegExPatternException(exception);
      }
    }

    results.sort(Comparator.comparing(JapaneseEntry::japanese));

    var msg = "Processed search using regular expression: " + regexPattern + ". Found ";
    msg += results.size() == ONLY_ONE_ENTRY_FOUND ? "1 entry." : results.size() + " entries.";
    LOGGER.log(Level.INFO, msg);

    return results;
  }

  /** {@inheritDoc} */
  @Override
  public List<JapaneseEntry> getEntries() throws IOException, SQLException {
    synchronized (entriesMap) {
      if (entriesMap.isEmpty()) {
        try (var statement = connection.prepareStatement(SELECT_ALL); ResultSet rs = statement.executeQuery()) {
          while (rs.next()) {
            var kanji = rs.getString(1);
            var reading = rs.getString(2);
            var translation = rs.getString(3);
            var entry = new JapaneseEntry(kanji, reading, translation);
            entriesMap.put(kanji, entry);
          }
        }
      }
      var entries = new ArrayList<>(entriesMap.values());
      entries.sort(Comparator.comparing(JapaneseEntry::japanese));
      return entries;
    }
  }

}
