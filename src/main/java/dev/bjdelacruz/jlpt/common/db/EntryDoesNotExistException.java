package dev.bjdelacruz.jlpt.common.db;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * An exception that is thrown if a user tries to remove or update a non-existing entry.
 * 
 * @author BJ Peter DeLaCruz
 */
public class EntryDoesNotExistException extends Exception {

  /**
   * Creates a new EntryDoesNotExistException instance.
   * 
   * @param entry The entry that does not currently exist in the database.
   */
  EntryDoesNotExistException(JapaneseEntry entry) {
    super("The following entry does not exist in the database: " + entry);
  }

}
