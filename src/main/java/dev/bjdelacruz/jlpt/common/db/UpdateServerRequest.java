package dev.bjdelacruz.jlpt.common.db;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * A request for updating an entry in the database on the server.
 *
 * @param newEntry The new entry to put in the database.
 * @param oldEntry The old entry to update.
 * @author BJ Peter DeLaCruz
 */
public record UpdateServerRequest(JapaneseEntry newEntry, JapaneseEntry oldEntry) implements ServerRequest {

  /**
   * Creates a new UpdateRequest.
   */
  public UpdateServerRequest {
    if (newEntry == null) {
      throw new IllegalArgumentException("New entry must not be null.");
    }
    if (oldEntry == null) {
      throw new IllegalArgumentException("Old entry must not be null.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Commands command() {
    return Commands.UPDATE;
  }
}
