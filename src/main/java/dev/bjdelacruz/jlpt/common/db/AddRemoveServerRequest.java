package dev.bjdelacruz.jlpt.common.db;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * A request for adding an entry to the database or removing an entry from the database.
 *
 * @param command The command, either add or remove.
 * @param entry   The entry to add or remove.
 * @author BJ Peter DeLaCruz
 */
public record AddRemoveServerRequest(Commands command, JapaneseEntry entry) implements ServerRequest {
}
