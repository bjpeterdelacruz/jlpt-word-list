package dev.bjdelacruz.jlpt.common.db;

/**
 * An exception that will be thrown when a user inputs an invalid regular expression pattern.
 * 
 * @author BJ Peter DeLaCruz
 */
public class InvalidRegExPatternException extends Exception {

  /**
   * Creates a new InvalidRegExPatternException.
   * 
   * @param e The exception that was thrown.
   */
  InvalidRegExPatternException(Exception e) {
    super(e);
  }

  /**
   * Creates a new InvalidRegExPatternException.
   * 
   * @param msg The message for this exception.
   */
  InvalidRegExPatternException(String msg) {
    super(msg);
  }

}
