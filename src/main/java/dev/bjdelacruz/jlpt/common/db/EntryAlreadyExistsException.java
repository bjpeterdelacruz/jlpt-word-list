package dev.bjdelacruz.jlpt.common.db;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * An exception that is thrown when a user tries to add an already existing entry to the database.
 * 
 * @author BJ Peter DeLaCruz
 */
public class EntryAlreadyExistsException extends Exception {

  /**
   * Creates a new EntryAlreadyExistsException.
   * 
   * @param entry The entry that already exists in the database.
   */
  EntryAlreadyExistsException(JapaneseEntry entry) {
    super("The following entry already exists in the database: " + entry);
  }

}
