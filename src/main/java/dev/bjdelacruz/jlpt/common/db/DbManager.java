package dev.bjdelacruz.jlpt.common.db;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * Contains methods for operations against the database file.
 * 
 * @author BJ Peter DeLaCruz
 */
public interface DbManager {

  /**
   * Adds an entry to the database. The entry <code>must</u> not already exist.
   * 
   * @param entry The entry to add.
   * @throws EntryAlreadyExistsException If the entry already exists in the database.
   * @throws IOException If there are problems sending the request from the client to the server.
   * @throws SQLException If there are problems adding an entry to the database.
   */
  void addEntry(JapaneseEntry entry) throws EntryAlreadyExistsException, IOException, SQLException;

  /**
   * Removes an entry from the database. The entry <u>must</u> already exist.
   * 
   * @param entry The entry to remove.
   * @throws EntryDoesNotExistException If the entry currently does not exist in the database.
   * @throws IOException If there are problems sending the request from the client to the server.
   * @throws SQLException If there are problems removing an entry from the database.
   */
  void removeEntry(JapaneseEntry entry) throws EntryDoesNotExistException, IOException, SQLException;

  /**
   * Updates an entry in the database. The entry <u>must</u> already exist.
   * 
   * @param newEntry The new entry to put in the database.
   * @param oldEntry The old entry that is currently in the database.
   * @throws EntryDoesNotExistException If the entry currently does not exist in the database.
   * @throws StaleEntryException If the entry to be updated has already been updated by another
   * user.
   * @throws IOException If there are problems sending the request from the client to the server.
   * @throws SQLException If there are problems updating the entry in the database.
   */
  void updateEntry(JapaneseEntry newEntry, JapaneseEntry oldEntry)
      throws EntryDoesNotExistException, StaleEntryException, IOException, SQLException;

  /**
   * Finds one or more entries in the database using the given regular expression pattern.
   * 
   * @param regexPattern The regular expression pattern to use for the search.
   * @return A list of entries, may be empty if none are found.
   * @throws ClassNotFoundException If the class for the object that was read in cannot be found.
   * @throws InvalidRegExPatternException If the regular expression is invalid.
   * @throws IOException If there are problems sending a command or reading in the results.
   */
  List<JapaneseEntry> find(String regexPattern) throws ClassNotFoundException, InvalidRegExPatternException,
          IOException;

  /**
   * @return A list of all entries in the database.
   * @throws ClassNotFoundException If the class for the object that was read in cannot be found.
   * @throws IOException If there are problems sending a command or reading in the results.
   * @throws SQLException If there are problems getting the entries in the database.
   */
  List<JapaneseEntry> getEntries() throws ClassNotFoundException, IOException, SQLException;

}
