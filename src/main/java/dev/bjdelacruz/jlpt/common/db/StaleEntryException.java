package dev.bjdelacruz.jlpt.common.db;

import dev.bjdelacruz.jlpt.common.datamodel.JapaneseEntry;

/**
 * An exception that is thrown when a user attempts to update an entry that has already been updated
 * by another user.
 * 
 * @author BJ Peter DeLaCruz
 */
public class StaleEntryException extends Exception {

  /**
   * Creates a new StaleEntryException.
   * 
   * @param entry The entry that was updated.
   */
  StaleEntryException(JapaneseEntry entry) {
    super("The following entry has already been updated by another user: " + entry);
  }

}
