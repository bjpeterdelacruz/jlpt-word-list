package dev.bjdelacruz.jlpt.common;

import dev.bjdelacruz.jlpt.server.main.ServerMain;

import java.io.File;

/**
 * This class contains constants that are used in the entire application.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class AppConstants {

  /** Do not instantiate this class. */
  private AppConstants() {
  }

  /** The <code>application.properties</code> file. */
  public static final File PROPERTIES_FILE = new File(System.getProperty("user.dir")
      + File.separator + "application.properties");

  /** Specifies the port to which clients should use to connect to the server. */
  public static final String SERVER_PORT_KEY = "server.port";

  /**
   * The server auto-start key. Specifies whether the server should be automatically started when
   * the application is running in server mode. The values for <code>server.location</code> and
   * <code>server.port</code> <b><u>must</u></b> be specified first. Otherwise, the server will have
   * to be manually started after these values are entered in {@link ServerMain}.
   */
  public static final String SERVER_AUTOSTART_KEY = "server.autostart";

  /** Specifies the location of the server when running in networked client mode. */
  public static final String NETWORKED_CLIENT_SERVER_LOCATION_KEY = "networked.server.location";

  /** Specifies the port on the server to which to connect when running in networked client mode. */
  public static final String NETWORKED_CLIENT_SERVER_PORT_KEY = "networked.server.port";

  /** The default locale for the application. */
  public static final String LOCALE = "locale";

  /** Size of log file (5 MB). */
  public static final int LOG_SIZE = 1000 * 1000 * 5;

  /** Number of log files to use. */
  public static final int LOG_ROTATION_COUNT = 1;

}
