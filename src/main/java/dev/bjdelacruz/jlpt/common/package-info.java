/**
 * Contains classes used by both the client and the server.
 */
package dev.bjdelacruz.jlpt.common;
