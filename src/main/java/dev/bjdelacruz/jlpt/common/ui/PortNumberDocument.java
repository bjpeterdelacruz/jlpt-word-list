/* 
 * PortNumberDocument.java
 * Version: 1.0
 * Date: 09/17/2012
 * 
 * Candidate: BJ Peter DeLaCruz
 * Prometric ID: 250772157
 * Candidate ID: OC0966755
 * 
 * 1Z0-855: Java Standard Edition 6 Developer Certified Master Assignment
 * 
 * This class is part of the Oracle Certified Master Java 6 Developer certification program, must
 * not be used out of this context, and must be used exclusively by Oracle Corporation.
 */
package dev.bjdelacruz.jlpt.common.ui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * A <code>Document</code> that doesn't allow non-numeric text in a <code>JTextField</code> and
 * restricts the length of the text to five characters.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
class PortNumberDocument extends PlainDocument {

  private static final int MAX_CHARS = 5;

  /**
   * Inserts a string into a <code>JTextField</code> if the string contains only numbers and is less
   * than six characters long.
   * 
   * @param offs The starting offset >= 0.
   * @param str The string to insert; does nothing with null/empty strings.
   * @param a The attributes for the inserted content.
   * @throws BadLocationException The given insert position is not a valid position within the
   * document.
   */
  @Override
  public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
    if (str == null) {
      return;
    }
    try {
      Integer.parseInt(str);
    }
    catch (NumberFormatException nfe) {
      return;
    }
    if (getLength() + str.length() > MAX_CHARS) {
      return;
    }
    super.insertString(offs, str, a);
  }

}
