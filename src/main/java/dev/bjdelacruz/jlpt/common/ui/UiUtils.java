package dev.bjdelacruz.jlpt.common.ui;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * Contains utility methods for modifying UI components.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class UiUtils {

  private static final String JPN_FLAG_ICON_URL = "images/jpn-flag.png";
  private static final String OFFLINE_ICON_URL = "images/offline.png";
  private static final String ONLINE_ICON_URL = "images/online.png";
  private static final String CONNECTING_ICON_URL = "images/connecting.png";
  private static final String STANDALONE_ICON_URL = "images/standalone.png";
  private static final String INIT_ICON_URL = "images/init.png";
  private static final String ABOUT_ICON_URL = "images/about.png";

  /** Do not instantiate this class. */
  private UiUtils() {
    // Empty constructor.
  }

  /**
   * Centers a component in the center and on top of its parent.
   * 
   * @param parent The parent of the component to center.
   * @param child The component to center.
   */
  public static void centerComponentOnParent(JFrame parent, JFrame child) {
    if (parent == null) {
      throw new IllegalArgumentException("Parent must not be null.");
    }
    if (child == null) {
      throw new IllegalArgumentException("Child must not be null.");
    }
    var topLeft = parent.getLocationOnScreen();
    var parentSize = parent.getSize();

    var childSize = child.getSize();

    int x, y;
    if (parentSize.width > childSize.width) {
      var width = parentSize.width - childSize.width;
      x = width / 2 + topLeft.x;
    }
    else {
      x = topLeft.x;
    }

    if (parentSize.height > childSize.height) {
      var height = parentSize.height - childSize.height;
      y = height / 2 + topLeft.y;
    }
    else {
      y = topLeft.y;
    }

    child.setLocation(x, y);
  }

  /**
   * Sets the properties of a frame such as its title and icon image.
   * 
   * @param frame The frame whose properties are to be set.
   * @param title The title of the frame.
   */
  public static void setFrameProperties(JFrame frame, String title) {
    if (frame == null) {
      throw new IllegalArgumentException("Frame must not be null.");
    }
    if (title == null || title.isBlank()) {
      throw new IllegalArgumentException("Title must not be null or an empty string.");
    }

    frame.setTitle(title);
    frame.setIconImage(getJapaneseFlagIcon());
    frame.setResizable(false);
  }

  /** @return The Japanese flag icon. */
  public static Image getJapaneseFlagIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(JPN_FLAG_ICON_URL)).getImage();
  }

  /** @return The offline icon. */
  public static ImageIcon getOfflineIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(OFFLINE_ICON_URL));
  }

  /** @return The online icon. */
  public static ImageIcon getOnlineIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(ONLINE_ICON_URL));
  }

  /** @return The connecting icon. */
  public static ImageIcon getConnectingIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(CONNECTING_ICON_URL));
  }

  /** @return The standalone mode icon. */
  public static ImageIcon getStandaloneModeIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(STANDALONE_ICON_URL));
  }

  /** @return The icon that is displayed when the database is initializing. */
  public static ImageIcon getInitIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(INIT_ICON_URL));
  }

  /** @return The icon for the About dialog box. */
  public static ImageIcon getAboutIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(ABOUT_ICON_URL));
  }

}
