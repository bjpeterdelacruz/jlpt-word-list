package dev.bjdelacruz.jlpt.common.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

/**
 * A custom JTextField that is used to enter a port number.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class PortTextField extends JTextField {

  /**
   * Creates a custom JTextField that is used to enter a port number.
   * 
   * @param listener The key listener to add to this text field.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public PortTextField(KeyListener listener) {
    if (listener == null) {
      throw new IllegalArgumentException("Listener must not be null.");
    }
    addKeyListener(listener);
    setDisabledTextColor(Color.BLACK);
    setColumns(5);
    setDocument(new PortNumberDocument());
    setMaximumSize(new Dimension(50, getPreferredSize().height));
  }

}
