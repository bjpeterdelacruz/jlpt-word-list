/**
 * Contains UI elements that both the client and server use.
 */
package dev.bjdelacruz.jlpt.common.ui;
